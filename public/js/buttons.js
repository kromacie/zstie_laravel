window.addEventListener('load', function () {
    var headerUser = $('.aboutUser');
    var panelUser = $('.panel');
    var awCaret = $('.aboutUser > i');

    headerUser.on('click', function()
    {
        $(this).toggleClass('clicked');
        awCaret.toggleClass('fa-caret-up');
        panelUser.slideToggle(300);
    });

    $(document).click(function (event) {
        if ($(event.target).closest(panelUser).length === 0 && $(event.target).closest(headerUser).length === 0 && headerUser.hasClass('clicked')) {
            headerUser.removeClass('clicked');
            awCaret.removeClass('fa-caret-up');
            panelUser.slideToggle(300);
        }
    });

    $('.userMobilePanelButton > i').on('click', function(){
        $('#asideUsContainer').toggle(200);
        $('#asideUsContainer').addClass('asideUsClicked');
    });

    var asideUsContainerButton = $('#asideUsContainer > i');

    asideUsContainerButton.on('click', function(){
        $('#asideUsContainer').toggle(200);
        $('#asideUsContainer').removeClass('asideUsClicked');
    });

    $(document).click(function(event){
        if($(event.target).closest($('#asideUsContainer')).length === 0 && $(event.target).closest($('.userMobilePanelButton > i')).length === 0 && $('#asideUsContainer').hasClass('asideUsClicked')){
            $('#asideUsContainer').removeClass('asideUsClicked');
            $('#asideUsContainer').toggle(200);
        }
    })
});
