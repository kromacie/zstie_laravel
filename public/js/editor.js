window.addEventListener('load', function () {
    $(function()
    {
        setInterval(watchCharRemains, 0);
        var titleLength, descriptionLength, titleRemain, descriptionRemain;

        function watchCharRemains()
        {
            titleLength = $('.formTitle input').val().length;
            descriptionLength = $('.formDescription input').val().length;
            titleRemain = (40 - titleLength);
            descriptionRemain = (100 - descriptionLength);
            $('.formTitle .charRemain').html('(Pozostało: '+titleRemain+'/40)');
            $('.formDescription .charRemain').html('(Pozostało: '+descriptionRemain+'/100)');
        }

        var editor = $('#resultText');

        var format = {
            'paragraph':'<p>',
            'bold':"bold",
            'italic':'italic',
            'left':'justifyLeft',
            'center':'justifyCenter',
            'right':'justifyRight',
            'justify':'justifyFull',
            'numericList':'insertOrderedList',
            'nonNumericList':'insertUnorderedList',
            'link':'CreateLink',
            'undo':'undo',
            'redo':'redo',
            'underline':'underline',
            'strike':'strikethrough',
            'fontSize':'fontSize',
            'fontName':'fontName',
            'foreColor':'foreColor'
        }

        editor.designMode = "on";
        $('.editorValue').on('click', function(){
            editor.focus();
            var name = $(this).attr('name');
            if(name != undefined){
                if(name || name != null || name != ""){
                    var block = format[`${name}`];
                    if(name == 'link'){
                        var url = prompt("Podaj adres: ", "http://");
                        document.execCommand(`${block}`,false, url);
                    }else if(name != "formatBlock"){
                        document.execCommand(`${block}`,false,null);
                    }else{
                        var title = $(this).attr('title');
                        var block = format[`${title}`];
                        document.execCommand(`formatBlock`,false,`${block}`);

                        if(title == "paragraph"){
                            $(this).css("color", "red");
                        }
                    }

                }
            }
        });

        $("#editor").submit( function() {
            $('<input />').attr('type', 'hidden')
                .attr('name', "content")
                .attr('value', editor.html())
                .appendTo('#editor');
            return true;
        });

        $('.selectors').on('change', function(e){
            editor.focus();
            var name = $(this).attr('name');
            if(name != undefined){
                if(name || name != null || name != ""){
                    var block = format[`${name}`];
                    document.execCommand(`${block}`,false,e.target.value);
                }
            }
        });

        $(window).on('beforeunload', function() {
            if(editor.length != 0 || titleLength != 0 || descriptionLength != 0){
                return "Na pewno chcesz opuścić stronę?";
            }
            return "none";
        });
    });

    $('#eventAdd').on('click', function(){
        if ($(this).is(":checked")){
            $('#eventDate').css('border', "1px solid red").attr("required", "").removeAttr("hidden");
        }
        else{
            $('#eventDate').css('border', '1px solid #DDD').removeAttr("required").attr("hidden", "");
        }
    })
});
