window.addEventListener('load', function () {
    var mobileButton = $('#navMobile');
    var navBox = $('#navWrapper');

    mobileButton.on('click', function()
    {
        navBox.slideToggle(100);
    });

    var navContainer = $('.nav > a');

    navContainer.on('click', function(e){
        if($(window).width() <= 768 && $(this).attr("name") != undefined)
        {
            e.preventDefault();
            $(this).next().slideToggle();
        }
    });
});
