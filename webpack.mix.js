let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    .copy('resources/assets/js/buttons.js', 'public/js')
    .copy('resources/assets/js/dynamicNav.js', 'public/js')
    .copy('resources/assets/js/editor.js', 'public/js')
    .copy('resources/assets/js/gallery.js', 'public/js')
    .js('resources/assets/js/app.js', 'public/js')
    .sass('resources/assets/sass/main.scss', 'public/css')
    //.copyDirectory('resources/assets/images', 'public/images') nie robic, obrazki z public images mogą zniknąć
    .copyDirectory('resources/assets/awesome/webfonts', 'public/webfonts')
    .sass('resources/assets/sass/editor/editor.scss', 'public/css');


