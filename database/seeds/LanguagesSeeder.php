<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LanguagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $locales = config('translatable.locales');

        foreach ($locales as $locale){
            DB::table('languages')->insert([
                'id' => $locale
            ]);
        }
    }
}
