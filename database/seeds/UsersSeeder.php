<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $id = DB::table('users')->insertGetId([
            'name' => 'Maciej',
            'email' => 'admin@admin.com',
            'password' => Hash::make('Admin12345'),
        ]);

        /** @var \App\User $user */
        $user = User::find($id);
        $user->assignRole('Admin');

        $id = DB::table('users')->insertGetId([
            'name' => 'Daniel',
            'email' => 'writer@admin.com',
            'password' => Hash::make('Admin12345'),
        ]);

        /** @var \App\User $user */
        $user = User::find($id);
        $user->assignRole('Writer');
    }
}
