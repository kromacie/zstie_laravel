<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->increments('id');
            $table->text('image');
            $table->boolean('is_event');
            $table->boolean('is_slide');
            $table->boolean('is_important');
            $table->boolean('is_published');
            $table->timestamp('calendar_date');
            $table->integer('user_id')->unsigned();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

        });

        Schema::create('news_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 40);
            $table->string('description', 100);
            $table->text('content');
            $table->string('language_id', 5)->index();
            $table->integer('news_id')->unsigned();

            $table->unique(['news_id','language_id']);
            $table->foreign('news_id')->references('id')->on('news')->onDelete('cascade');
            $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news_translations');
        Schema::dropIfExists('news');
    }
}
