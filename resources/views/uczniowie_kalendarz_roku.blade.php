@extends('layouts.subsite', ['panel' => 'includes.student'])
@section('content2')

<div class="content">
	<p>Kalendarz roku szkolnego 2018/2019</p><hr>
	<p>Harmonogram spotkań z rodzicami w roku szkolnym 2017/2018 - <a href="{{ url('pdf/spotkania_rodzice_2017.pdf') }}" target="_blank">Plik w formacie PDF</a></p>
	<p><b>Kalendarz roku szkolnego 2017/2018 w Zespole Szkół Teleinformatycznych i Elektronicznych we Wrocławiu</b> (podstawa prawna: Rozporządzenie Ministra Edukacji Narodowej i Sportu z dnia 18 kwietnia 2002r. w sprawie organizacji roku szkolnego – Dz. U. Nr 46 poz. 432 z późn. zm. oraz Komunikat Dyrektora Centralnej  Komisji Egzaminacyjnej ).</p>
	<p><a href="{{ url('pdf/kalendarium_2017.pdf') }}" target="_blank">Plik w formacie PDF</a></p>
</div>

@endsection