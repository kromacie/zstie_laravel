@extends('layouts.subsite', ['panel' => 'includes.school'])
@section('content2')
<div class="content">
    <p>Czasy przedwojenne</p>
    <span style="font-weight: bold; text-transform: uppercase;">Berufsschule - Clausewitzstrasse</span><hr>
    <p>Gmach szkoły zawodowej dla dziewcząt powstał w latach 1929-1930 w miejscu dawnej olejarni. Autorem projektu był Hugo Althoff (miejski radca budowlany w latach 1926-29) we współpracy z Carlem Zollerem. Budynek, w stylu modernistycznym, założono na planie litery T, choć były plany dobudowania kolejnego skrzydła wzdłuż ul. Worcella. Budynek ma cztery kondygnacje przy czym ostatnia jest cofnięta tworząc dwa tarasy po bokach centralnie usytuowanej klatki schodowej.</p>
    <p>Parter wyłożony cegłą klinkierową kontrastuje z błękitno-szarym (kiedyś) tynkiem pozostałych pięter. Pomimo braku sali gimnastycznej szkoła była dobrze wyposażona. Zajęcia teoretyczne odbywały się w 24 salach lekcyjnych, a praktyczne w czterech kuchniach, trzech pralniach i prasowalniach. Uczniowie mieli dostęp także do biblioteki, świetlicy, stołówki oraz gabinetu lekarskiego z pokojem rentgenowskim. Na parterze znajdowały się mieszkania woźnego i palacza.</p>
</div>
<div class="content">
    <p>II Wojna Światowa</p><hr>
    <p>Obóz pracy (obóz przejściowy dla cudzoziemców). Przetrzymywano w nim według różnych źródeł od 3 tys. do 10 tys.) – robotników przymusowych z <b>Polski</b>, <b>Czech</b>, <b>Ukrainy</b>, <b>Węgier</b>, <b>Francji</b>, <b>Jugosławii</b>.</p>
    <p>Był to jeden z większych obozów tego typu we Wrocławiu. Obóz istniał do wiosny 1945 r. W czasie oblężenia został zbombardowany w kwietniu 1945 r., co spowodowało duże straty wśród ludzi. Ocalałych przeniesiono do obozu na Sołtysowicach. Tragiczne losy przetrzymywanych w obozie na ul. Hauke-Bosaka więźniów upamiętnia tablica wmurowana w 1970 r.</p>
</div>
<div class="content">
    <p>Powojenne zmiany</p><hr>
    <p>Po II Wojnie Światowej budynek powrócił do pierwotnego przeznaczenia. Od IX 1945 po dzień dzisiejszy 21 pod adresem gen. Józefa Hauke-Bosaka goszczą różne szkoły.</p>
    <p>Kolejno: Państwowe Gimnazjum (do IX 1952) i Liceum Techniczne, Liceum Mechaniczno-Elektryczne, Liceum Elektryczne, Technikum Mechaniczne i Technikum Energetyczne, Technikum (z Zasadniczą Szkołą Zawodową) Energetyczne i Technikum Przemysłowo-Pedagogiczne, Technikum Elektryczne i Pedagogiczne Studium Techniczne (wraz ze szkołą policealną), Zespół Szkół Elektrycznych i Technikum Łączności, od IX 1987 do VIII 2002 Zespół Szkół Łączności im. Romualda Traugutta.</p>
    <p>Rok 2002 we wrocławskiej edukacji przyniósł wiele zmian. W ich następstwie zlikwidowano dwie szkoły: Zespół Szkół Łączności im. Romualda Traugutta oraz Zespół Szkół Elektronicznych im. Jana Henryka Dąbrowskiego z siedzibą na ul. Świerczewskiego (po przemianowaniu Piłsudskiego) 27. W ich miejsce powołano Zespół Szkół Teleinformatycznych i Elektronicznych i ulokowano w budynku dawnego ZSŁ.</p>
</div>
<div class="content">
    <p>Dzieje współczesne</p><hr>
    <p>1 IX 2002 r. jest początkiem istnienia Zespołu Szkół Teleinformatycznych i Elektronicznych. Początkowo w skład Zespołu wchodziły wszystkie odziedziczone typy szkół, aż do ich wygaszenia.</p>
    <p>Ostatecznie w ramach ZSTiE ukształtowały się: Technikum nr 7 z zawodami: technik teleinformatyk,  technik telekomunikacji, a od 1 IX 2009 r. technik informatyk; Liceum Profilowane nr XIX z profilami: zarządzanie informacja i elektronicznym; Zasadnicza Szkoła Zawodowa nr 1 z zawodami: monter elektronik oraz moner sieci i urządzeń telekomunikacyjnych; Szkoła Policealna nr 5 z zawodami: technik informatyk i technik telekomunikacji.</p>
    <p>2 X 2009 r. Zespół Szkół Teleinformatycznych i Elektronicznych we Wrocławiu otrzymał imię Polskich Zwycięzców Enigmy.</p>
</div>
@endsection