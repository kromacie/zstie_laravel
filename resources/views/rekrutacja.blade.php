@extends('layouts.subsite', ['panel' => 'includes.recruitment'])
@section('content2')
<div class="content">
    <p>Warunki rekrutacji do klas pierwszych</p><hr>
    <p><a href="{{ url('/pdf/tabela_rekrutacja_2017.pdf') }}" target="_blank">Warunki rekrutacji</a> do klas I-wszych Zespołu Szkół Teleinformatycznych i Elektronicznych we Wrocławiu na rok szkolny 2017/2018.</p>
</div>
<div class="content">
	<p>Nauczane języki</p><hr>
	<p>Na podstawie Zarządzenia dyrektora Nr 4/2013:<br>§ 10 - W szkole prowadzone sę zajęcia z zakresu języka obcego nowożytnego: <span name="att">języka angielskiego</span> i <span name="att">języka niemieckiego</span>. Przydział do grup ze względu na kontynuację i stopień zaawansowania następuje na podstawie zapisu na świadectwie ukończenia gimnazjum.</p>
</div>
<div class="content">
	<p>Skierowania lekarskie</p><hr>
	<p>Zgodnie z <a href="https://www.kuratorium.wroclaw.pl/wp-content/uploads/2017/04/zarzadzenie.pdf" target="_blank">Zarządzeniem nr 12/2017</a> DKO z dnia 28 marca 2017 r. w sprawie harmonogramu czynności w postępowaniu rekrutacyjnym oraz postępowaniu uzupełniającym do szkoły ponadgimnazjalnej na rok szkolny 2017/2018, zaświadczenie lekarskie zawierające orzeczenie o braku przeciwwskazań zdrowotnych do podjęcia praktycznej nauki zawodu, jest dokumentem niezbędnym do przyjęcia do technikum i branżowej szkoły zawodowej. Skierowania do przychodni medycyny pracy można pobierać w sekretariacie szkoły, w momencie zakwalifikowania kandydata do kształcenia w danym zawodzie.<br>Lista przychodni, w których można wykonać bezpłatnie badania uczniów w 2017 r. <br><a href="{{ url('/pdf/lista_przychodni_2017.pdf') }}" target="_blank">Plik</a> do pobrania w formacie PDF.</p>
</div>
@endsection