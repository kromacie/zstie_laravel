@extends('layouts.main')
@section('extra_styles')
    <link rel="stylesheet" type="text/css" href="{{ push('css/editor.css', 'style') }}">
@endsection
@section('content')
    <div id="editorWrapper">
        <p>{{ $news->title }}</p>
        <p>{{ $news->description }}</p>
        <img src="{{ push('storage/' . $news->image, 'image') }}" width="100%">
        <p>{!! $news->content !!}</p>
        @if(auth()->check())
            @can('edit articles')
                <a href="{{ route('news.edit', ['id' => $news->id]) }}">Edytuj</a>
            @endcan
            @can('delete articles')
                <a href="{{ route('news.delete', ['id' => $news->id]) }}">Usuń</a>
            @endcan
        @endif
    </div>
@endsection
