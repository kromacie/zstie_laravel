@extends('layouts.main')
@section('content')
<div id="content-wrapper">
    <div class="content">
        <div id="gallery_folders">
            <div class="folder">
                <a href="{{ route('school.gallery.zstie') }}">
                    <div class="gallery-header">
                        <span>Zespół szkół teleinformatycznych i elektronicznych</span>
                    </div>
                    <img src="{{ push('images/gallery/ZSTiE/3.jpg', 'image') }}">
                </a>
            </div>
            <div class="folder">
                <a href="#">
                    <div class="gallery-header">
                        <span>Radosna Parada Niepodległości 2016</span>
                    </div>
                    <img src="{{ push('images/gallery/Radosna_Parada_Niepodleglosci/1.jpg', 'image') }}">
                </a>
            </div>
            <div class="folder">
                <a href="#">
                    <div class="gallery-header">
                        <span>Spotkanie szkół we Wrocławiu 2016</span>
                    </div>
                    <img src="{{ push('images/gallery/Spotkanie_szkol/1.jpg', 'image') }}">
                </a>
            </div>
            <div class="folder">
                <a href="#">
                    <div class="gallery-header">
                        <span>IV Seminarium Samorządów Uczniowskich</span>
                    </div>
                    <img src="{{ push('images/gallery/IV_Seminarium_Samorzadow_Uczniowskich/1.JPG', 'images') }}">
                </a>
            </div>
        </div>
    </div>
</div>
@endsection