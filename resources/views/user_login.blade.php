@extends('layouts.main')
<!-- <div class="content">
	<form action="#" method="post">
		<input type="text" name="login" placeholder="Podaj swój login lub email..." required>
		<input type="text" name="password" placeholder="Podaj hasło..." required>
		<input type="submit" name="user_log" value="Zaloguj">
		<input type="hidden" name="token" value="TUTAJ TOKEN">
	</form>
	<a href="">Nie pamiętasz hasła?</a>
</div> -->

@section('content')
    <form id="formSignReg" action="{{ route('login') }}" method="post">
        @csrf
        <div id="formWrapper">
        <table id="formTable">
            <tr>
                <td>
                    <p class="formSentence sentence">Email:</p>
                </td>
                <td><input name="email" value="{{ old('email') }}" type="text" autofocus required placeholder="Podaj swój email..."></td>
            </tr>
            <tr>
                <td>
                    <p class="formSentence sentence">Hasło:</p>
                </td>
                <td><input name="password" value="{{ old('password') }}" type="password" required placeholder="Wpisz swoje hasło..."></td>
            </tr>
        </table>
        <div class="checkbox">
            <input id="saveCheck" type="checkbox" name="remember" {{ old('remember') == 'on' ? 'checked' : '' }}>
            <label for="saveCheck" class="small-caps">zapamiętaj mnie</label>
        </div><br>
        <a href="">Nie pamiętasz hasła?</a>
        <hr>
        <input type="submit" value="Zaloguj się">
        </div>
    </form>
@endsection

