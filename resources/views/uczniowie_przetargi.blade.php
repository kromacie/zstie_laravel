@extends('layouts.subsite', ['panel' => 'includes.student'])
@section('content2')

<div class="content">
	<p>Ogłoszenie wyników postępowania prowadzonego w trybie zapytania o cenę na realizację zadania pod nazwą: Zakup wyposażenia dydaktycznego pracowni komputerowych.</p><hr>
	<p>Zamawiający informuje o wyborze najkorzystniejszej ofert: <b>ATUT Komputer, ul. Armii Krajowej 10a/2, 50-541 Wrocław</b></p><hr>
	<p>Pełna treść ogłoszenia w: <a href="{{ url('pdf/wyniki_s412.pdf')" target="_blank">pliku PDF</a></p>
</div>

@endsection