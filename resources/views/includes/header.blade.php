<div id="headerWrapper">
	<div id="headerContainer">
		<div class="titleContainer">
			<a href="{{ route('home') }}"><span>ZSTiE we Wrocławiu</span></a>
		</div>
		@if(auth()->check())
		<div class="userContainer">
			<p class="aboutUser">
				<span>{{ $user->name }}</span>
				<i class="fas fa-caret-down"></i>
			</p>
			<div class="panel">
				@can('create users')
					<a href="{{ route('register') }}" attr="user">Rejestracja</a>
				@endcan
				<a href="{{ route('profile') }}" attr="user">Mój profil</a>
				@can('publish articles')
					<a href="{{ route('news.unpublished.show') }}" attr="user">Kolejka artykułów</a>
				@endcan
				{{--<label for="selectStyle">--}}
					{{--Styl:--}}
					{{--<select name="selectStyle">--}}
						{{--<option value="mainStyle">standard</option>--}}
						{{--<option value="shadowStyle">grayShades</option>--}}
						{{--<option value="blueStyle">lightBlue</option>--}}
						{{--<option value="purpleMagic">purpleMagic</option>--}}
					{{--</select>--}}
				{{--</label>--}}
				<a href="{{ route('logout') }}" attr="user">Wyloguj</a>
			</div>
		</div>
		@endif
		<div class="userMobilePanelButton">
			<i class="fas fa-caret-left"></i>
		</div>
	</div>
</div>