<div id="newsWrapper">
    @foreach($news as $single)
    <div class="news">
        <p class="title"><a href="#">{{ $single->title }}</a></p>
        <div class="details">
            <div class="author">{{ $single->user->name }}</div>
            <div class="date">{{ $single->created_at }}</div>
        </div>
        <div class="picture"><a href="{{ route('news.show', ['id' => $single->id]) }}"><img src="{{ push('storage/'.$single->image, 'image') }}" alt="slide-1"></a></div>
        <div class="description">
            <p>{{ $single->description }}</p>
        </div>
    </div>
    @endforeach
</div>
