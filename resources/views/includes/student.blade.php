<p>Uczniowie</p>
<ol>
    <p>Organizacja pracy</p>
    <li><a href="#" target="_blank"><span>Plan lekcji</span><i class="fa fa-angle-right"></i></a></li>
    <li><a href="#" target="_blank"><span>Zastępstwa</span><i class="fa fa-angle-right"></i></a></li>
    <li><a href="{{ route('students.calendar.year') }}"><span>Kalendarz roku 17/18</span><i class="fa fa-angle-right"></i></a></li>
    <li><a href="{{ route('students.books') }}"><span>Podręczniki</span><i class="fa fa-angle-right"></i></a></li>
</ol>
<ol>
    <p>Egzaminy</p>
    <li><a href="{{ route('students.exam.mature') }}"><span>Egzamin maturalny</span><i class="fa fa-angle-right"></i></a></li>
    <li><a href="{{ route('students.exam.tech') }}"><span>Egzamin zawodowy</span><i class="fa fa-angle-right"></i></a></li>
    <li><a href="{{ route('students.exam.results') }}"><span>Wyniki egzaminów </span><i class="fa fa-angle-right"></i></a></li>
</ol>
<ol>
    <p>Dodatkowe</p>
    <li><a href="{{ route('students.calendar.events') }}"><span>Kalendarz imprez</span><i class="fa fa-angle-right"></i></a></li>
    <li><a href="{{ route('students.lessons') }}"><span>Zajęcia pozalekcyjne</span><i class="fa fa-angle-right"></i></a></li>
    <li><a href="{{ route('students.projects') }}"><span>Projekty</span><i class="fa fa-angle-right"></i></a></li>
</ol>
<ol>
    <p>Informacje prawne</p>
    <li><a href="{{ route('students.laws') }}"><span>Akty prawne</span><i class="fa fa-angle-right"></i></a></li>
    <li><a href="{{ route('students.tenders') }}"><span>Przetargi</span><i class="fa fa-angle-right"></i></a></li>
</ol>