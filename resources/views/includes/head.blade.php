
<meta charset="utf-8">
	<title>@yield('title', 'ZSTiE')</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta property="og:title" content="nazwa strony">
	<meta property="og:site_name" content="nazwa strony">
	<meta property="og:type" content="website">
	<meta property="og:locate" content="pl_PL">
	<meta property="og:image" content="{{ push('favicon.ico', 'image') }}">
	<meta property="og:description" content="opis strony">
	<meta name="title" content="nazwa strony">
	<meta name="description" content="opis strony">
	<!-- <meta name="ROBOTS" content="noodp"> -->
	<link rel="icon" type="image/png" href="{{ push('images/header/logo.png', 'image') }}">
	<link rel="stylesheet" type="text/css" href="{{ push('css/reset.css', 'style') }}">
	<link rel="stylesheet" type="text/css" href="{{ push('css/main.css', 'style') }}">
	@yield('extra_styles')
	<!--[if lt IE 9]> <script src="{{ push('https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js', 'script') }}"></script><![endif]-->
