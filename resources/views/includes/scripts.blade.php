<script src="{{ push('js/app.js', 'script') }}"></script>
<script src="{{ push('js/dynamicNav.js', 'script') }}"></script>
<script src="{{ push('js/buttons.js', 'script') }}"></script>
@yield('extra_scripts')