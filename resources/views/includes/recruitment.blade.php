<p>Rekrutacja</p>
<ol>
    <p>Dla kandydata</p>
    <li><a href="{{ route('recruitment.conditions') }}"><span>Warunki przyjęcia</span><i class="fa fa-angle-right"></i></a></li>
	<li><a href="{{ route('recruitment.languages') }}"><span>Nauczane języki</span><i class="fa fa-angle-right"></i></a></li>
	<li><a href="{{ route('recruitment.doctor') }}"><span>Skierowania lekarskie</span><i class="fa fa-angle-right"></i></a></li>
</ol>
<ol>
    <p>Ulotka szkoły</p>
    <li><a href="{{ url('/pdf/promo_2017.pdf') }}" target="_blank"><span>ulotka</span><i class="fa fa-angle-right"></i></a></li>
</ol>