<div id="navMobile"><i class="fas fa-bars"></i><span class="small-caps">menu</span></div>
<div id="navWrapper">
	<div class="nav">
		<a href="{{ route('home') }}">ZSTiE</a>
	</div>
	<div class="nav">
		<a href="{{ route('news') }}">Nowości</a>
	</div>
	<div class="nav">
		<a href="{{ route('recruitment') }}" name="subAddon">Rekrutacja<i class="fas fa-angle-down"></i></a>
		<div class="subNav">
			<div class="subNavCol" name="joinUs">
				<p>Dla kandydata</p>
				<img src="{{ push('images/nav/join_us.jpeg', 'image') }}" alt="dla kandydata">
				<li><a href="{{ route('recruitment.conditions') }}"><span>Warunki przyjęcia</span> <i class="fa fa-angle-right"></i></a></li>
				<li><a href="{{ route('recruitment.languages') }}"><span>Nauczane języki</span> <i class="fa fa-angle-right"></i></a></li>
				<li><a href="{{ route('recruitment.doctor') }}"><span>Skierowania lekarskie</span> <i class="fa fa-angle-right"></i></a></li>
				
			</div>
			<div class="subNavCol" name="joinUs">
				<p>Ulotka informacyjna o szkole</p>
				<li name="promo"><a href="{{ asset('pdf/promo_2017.pdf') }}" target="_blank"><span>ulotka (plik pdf)</span> <i class="fa fa-angle-right"></i></a></li>
				<a href="{{ asset('pdf/promo_2017.pdf') }}" target="_blank"><img src="{{ push('images/nav/leaflet1.png', 'image') }}" alt="ulotka informacyjna o szkole"></a>
			</div>
		</div>
	</div>
	<div class="nav">
		<a href="{{ route('school') }}" name="subAddon">Szkoła<i class="fas fa-angle-down"></i></a>
		<div class="subNav">
			<div class="subNavCol">
				<p>o nas</p>
				<img src="{{ push('images/nav/school.jpg', 'image') }}" alt="o nas">
				<li><a href="{{ route('school.history') }}"><span>Historia</span> <i class="fa fa-angle-right"></i></a></li>
				<li><a href="{{ route('school.patrons') }}"><span>Nasi patroni</span> <i class="fa fa-angle-right"></i></a></li>
				<li><a href="{{ route('school.gallery') }}"><span>Galeria</span> <i class="fa fa-angle-right"></i></a></li>
				<li><a href="{{ route('school.contact') }}"><span>Kontakt</span> <i class="fa fa-angle-right"></i></a></li>
			</div>
			<div class="subNavCol">
				<p>kierunki</p>
				<img src="{{ push('images/nav/directions.jpeg', 'image') }}" alt="kierunki">
				<li><a href="{{ route('school.it') }}"><span>Technik informatyk</span> <i class="fa fa-angle-right"></i></a></li>
				<li><a href="{{ route('school.et') }}"><span>Technik elektronik</span> <i class="fa fa-angle-right"></i></a></li>
				<li><a href="{{ route('school.e') }}"><span>Elektronik</span> <i class="fa fa-angle-right"></i></a></li>
			
			</div>
			<div class="subNavCol">
				<p>kadra</p>
				<img src="{{ push('images/nav/personnel.jpeg', 'image') }}" alt="kadra pedagogiczna">
				<li><a href="{{ route('school.teachers') }}"><span>Kadra pedagogiczna</span> <i class="fa fa-angle-right"></i></a></li>
				<li><a href="http://www.pedagodzy.info/" target="_blank"><span>Pedagodzy</span> <i class="fa fa-angle-right"></i></a></li>
				<li><a href="{{ route('school.parents') }}"><span>Rada rodziców</span> <i class="fa fa-angle-right"></i></a></li>
				<li><a href="http://ahistorytoshare.vizz.pl/samorzadzstie/"><span>Samorząd uczniowski</span> <i class="fa fa-angle-right"></i></a></li>
			</div>
			<div class="subNavCol">
				<p>biblioteka</p>
				<img src="{{ push('images/nav/library.jpeg', 'image') }}" alt="biblioteka">
				<li><a href="{{ route('school.library') }}"><span>O bibliotece</span> <i class="fa fa-angle-right"></i></a></li>
				<li><a href="http://bibliotekazstie.blogspot.com/" target="_blank"><span>Blog</span> <i class="fa fa-angle-right"></i></a></li>
				<li><a href="http://prolib.edu.wroclaw.pl/cgi-bin/wspd_cgi.sh/wo2_logout.p?errorSes=1&ln=pl&IDBibl=265&ID1=LLMJOPMINQBHNIQQHO" target="_blank"><span name="att">Katalog (nie działa)</span> <i class="fa fa-angle-right"></i></a></li>
				<li><a href="https://bibliotekazstie.blogspot.com/p/narodowy-program-rozwoju-czytelnictwa.html" target="_blank"><span>Program Rozwoju Czyt.</span><i class="fas fa-angle-right"></i></a></li>
			</div>
		</div>
	</div>
	<div class="nav">
		<a href="{{ route('students') }}" name="subAddon">Uczniowie<i class="fas fa-angle-down"></i></a>
		<div class="subNav">
			<div class="subNavCol">
				<p>Organizacja pracy</p>
				<img src="{{ push('images/nav/calendar.jpeg', 'image') }}" alt="organizacja pracy">
				<li><a href="#" target="_blank"><span>Plan lekcji</span> <i class="fa fa-angle-right"></i></a></li>
				<li><a href="#" target="_blank"><span>Zastępstwa</span> <i class="fa fa-angle-right"></i></a></li>
				<li><a href="{{ route('students.calendar.year') }}"><span>Kalendarz roku 17/18</span> <i class="fa fa-angle-right"></i></a></li>
				<li><a href="{{ route('students.books') }}"><span>Podręczniki</span> <i class="fa fa-angle-right"></i></a></li>
			</div>
			<div class="subNavCol">
				<p>Egzaminy</p>
				<img src="{{ push('images/nav/exams.jpeg', 'image') }}" alt="egzaminy">
				<li><a href="{{ route('students.exam.mature') }}"><span>Egzamin maturalny</span> <i class="fa fa-angle-right"></i></a></li>
				<li><a href="{{ route('students.exam.tech') }}"><span>Egzamin zawodowy</span> <i class="fa fa-angle-right"></i></a></li>
				<li><a href="{{ route('students.exam.results') }}"><span>Wyniki egzaminów </span> <i class="fa fa-angle-right"></i></a></li>
			</div>
			<div class="subNavCol">
				<p>Dodatkowe</p>
				<img src="{{ push('images/nav/optional.jpeg', 'image') }}" alt="dodatkowe">
				<li><a href="{{ route('students.calendar.events') }}"><span>Kalendarz imprez</span> <i class="fa fa-angle-right"></i></a></li>
				<li><a href="{{ route('students.lessons') }}"><span>Zajęcia pozalekcyjne</span> <i class="fa fa-angle-right"></i></a></li>
				<li><a href="{{ route('students.projects') }}"><span>Projekty</span> <i class="fa fa-angle-right"></i></a></li>
			</div>
			<div class="subNavCol">
				<p>Informacje prawne</p>
				<img src="{{ push('images/nav/law.jpeg', 'image') }}" alt="informacje prawne">
				<li><a href="{{ route('students.laws') }}"><span>Akty prawne</span> <i class="fa fa-angle-right"></i></a></li>
				<li><a href="{{ route('students.tenders') }}"><span>Przetargi</span> <i class="fa fa-angle-right"></i></a></li>
			</div>
		</div>
	</div>
	<div class="nav">
		<a href="{{ route('school.gallery') }}">Galeria</a>
	</div>
	<div class="nav">
		<a href="{{ route('contact') }}">Kontakt</a>
	</div>
</div>