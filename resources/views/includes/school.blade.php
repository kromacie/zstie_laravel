<p>Szkoła</p>
<ol>
    <p>O nas</p>
    <li><a href="{{ route('school.history') }}"><span>Historia</span><i class="fa fa-angle-right"></i></a></li>
    <li><a href="{{ route('school.patrons') }}"><span>Nasi patroni</span><i class="fa fa-angle-right"></i></a></li>
    <li><a href="{{ route('school.gallery') }}"><span>Galeria</span><i class="fa fa-angle-right"></i></a></li>
    <li><a href="client/front/subsites/kontakt.php"><span>Kontakt</span><i class="fa fa-angle-right"></i></a></li>
</ol>
<ol>
    <p>Kierunki</p>
    <li><a href="{{ route('school.it') }}"><span>Technik informatyk</span><i class="fa fa-angle-right"></i></a></li>
    <li><a href="{{ route('school.et') }}"><span>Technik elektronik</span><i class="fa fa-angle-right"></i></a></li>
        <li><a href="{{ route('school.e') }}"><span>Elektronik</span><i class="fa fa-angle-right"></i></a></li>
</ol>
<ol>
    <p>Kadra</p>
    <li><a href="{{ route('school.teachers') }}"><span>Kadra pedagogiczna</span><i class="fa fa-angle-right"></i></a></li>
    <li><a href="http://www.pedagodzy.info/" target="_blank"><span>Pedagodzy</span><i class="fa fa-angle-right"></i></a></li>
    <li><a href="{{ route('school.parents') }}"><span>Rada rodziców</span><i class="fa fa-angle-right"></i></a></li>
    <li><a href="http://ahistorytoshare.vizz.pl/samorzadzstie/" target="_blank"><span>Samorząd uczniowski</span><i class="fa fa-angle-right"></i></a></li>
</ol>
<ol>
    <p>Biblioteka</p>
    <li><a href="{{ route('school.library') }}"><span>O bibliotece</span><i class="fa fa-angle-right"></i></a></li>
    <li><a href="http://bibliotekazstie.blogspot.com/" target="_blank"><span>Blog</span><i class="fa fa-angle-right"></i></a></li>
    <li><a href="http://prolib.edu.wroclaw.pl/cgi-bin/wspd_cgi.sh/wo2_logout.p?errorSes=1&ln=pl&IDBibl=265&ID1=LLMJOPMINQBHNIQQHO" target="_blank"><span name="att">Katalog (nie działa)</span><i class="fa fa-angle-right"></i></a></li>
    <li><a href="https://bibliotekazstie.blogspot.com/p/narodowy-program-rozwoju-czytelnictwa.html" target="_blank"><span>Program Rozwoju Czyt.</span></a><i class="fas fa-angle-right"></i></li>
</ol>