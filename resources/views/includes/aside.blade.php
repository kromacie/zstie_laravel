<div id="asideUsContainer">
	<i class="fas fa-times"></i>
	@if(auth()->check())
		<p class="userName">Fenikstfb</p>
		<div class="userPanel">
			<li><a href="{{ route('profile') }}" attr="user">Mój profil</a></li>
			<li><a href="{{ route('news.unpublished.show') }}" attr="user">Kolejka artykułów (admin)</a></li>
			<li>
				<label for="selectStyle">
					Styl:
					<select id="selectStyle" name="selectStyle">
						<option value="mainStyle">standard</option>
						<option value="shadowStyle">grayShades</option>
						<option value="blueStyle">lightBlue</option>
						<option value="purpleMagic">purpleMagic</option>
					</select>
				</label>
			</li>
			<li><a href="{{ route('logout') }}" attr="user">Wyloguj</a></li>
		</div>
	@endif
</div>