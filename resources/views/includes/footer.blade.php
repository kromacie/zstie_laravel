<div id="footerWrapper">
	<div id="footerContainer">
		<div class="footerContactContainer">
			<a href="https://www.facebook.com/zstie.wroclaw/" target="_blank"><i class="fab fa-facebook-f"></i></a>
			<a href="https://www.youtube.com/channel/UCvDAX7bl7MJbdlD7XYYYmVw" target="_blank"><i class="fab fa-youtube"></i></a>
		</div>
		<div class="moreAbout">
			<a href="{{ route('about.us') }}">O nas</a>
			<a href="{{ route('creators') }}">Twórcy</a>
			<a href="{{ route('cookies') }}">Cookies</a>
		</div>
		<div class="copyrights">
			&copy; 2018 ZSTiE.pl
		</div>
	</div>
</div>