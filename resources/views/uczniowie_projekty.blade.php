@extends('layouts.subsite', ['panel' => 'includes.student'])
@section('content2')

<div class="content">
    <p>projekty</p><hr>
    <li><a href="{{ route('events.change') }}">Wymiana młodzieży z Eckener Schule we Flensburgu</a></li>
    <li><a href="{{ route('events.volunter') }}">Wolontariat</a></li>
    <li><a href="{{ route('events.school') }}">Szkoła w mieście</a></li>
    <li><a href="http://www.dolnoslaski.szs.pl/news/list/page/3" target="_blank">Szkolny klub sportowy</a></li>
    <li><a href="https://blogiceo.nq.pl/wfwzstie/" target="_blank">WF z klasą - blog</a></li>
    <li><a href="https://historiazstie.wordpress.com/" target="_blank">Rocznice historyczne - blog</a></li>
</div>

@endsection