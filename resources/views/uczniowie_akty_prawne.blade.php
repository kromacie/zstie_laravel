@extends('layouts.subsite', ['panel' => 'includes.student'])
@section('content2')


<div class="content">
    <p>Akty prawne</p><hr>
    <p><a href="{{ url('pdf/obwieszczenie_statutu_2015.pdf') }}" target="_blank">Obwieszczenie </a>Dyrektora Szkoły dotyczące znowelizowanego Statutu ZSTiE</p>
    <p><a href="{{ url('pdf/statut_zstie_2016.pdf') }}" target="_blank">Status </a>ZSTiE - tekst ujednolicony w formacie PDF</p>
    <p><a href="{{ url('pdf/nowelizacja_statut_2015.pdf') }}" target="_blank">Nowelizacja </a>Statutu ZSTiE w formacie PDF</p>
    <p><a href="{{ url('pdf/reg_rady_ped_2017.pdf') }}" target="_blank">Regulamin Rady Pedagogicznej </a>ZSTiE w formacie PDF</p>
    <p><a href="{{ url('pdf/reg_sam_ucz_2017.pdf') }}" target="_blank">Regulamin Samorządu Uczniowskiego </a>ZSTiE w formacie PDF</p>
    <p><a href="{{ url('pdf/reg_rodzicow_2017.pdf') }}" target="_blank">Regulamin Rady Rodziców </a>ZSTiE w formacie PDF</p>
    <p><a href="{{ url('pdf/koncepcja_2013_2018.pdf') }}" target="_blank">Koncepcja funkcjonowania </a>i rozwoju ZSTiE w latach 2013-2018 w formacie PDF</p>
    <p><a href="{{ url('pdf/procedury_kryzysowe_2015.pdf') }}" target="_blank">Zestaw procedur postępowania w sytuacjach kryzysowych i trudnych wychowawczo </a>i rozwoju ZSTiE w latach 2013-2018 w formacie PDF</p>
    <p><a href="{{ url('pdf/program_wychowawczy_2016_2017.pdf') }}" target="_blank">Program wychowawczy </a>dla ZSTiE na rok szkolny 2016/2017</p>
</div>

@endsection