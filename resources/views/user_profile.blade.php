@extends('layouts.main')

@section('content')
	<div class="content">
		<p>Twój profil</p><hr>
		<div id="userProfileContainer">
			<div class="userContent">
				<span>Nazwa użytkownika: <span name="userValue">{{ $user->name }}</span></span><span><a href="#">Zmień</a></span>
			</div>
			<div class="userContent">
				<span>Adres email użytkownika: <span name="userValue">{{ $user->email }}</span></span><span><a href="#">Zmień</a></span>
			</div>
			<div class="userContent">
				<span>Twoja pozycja na forum:
					@foreach($user->roles as $role)
						<span name="userValue">{{ $role->name }}</span>
					@endforeach
				</span>
				<span><a href="#">Zmień hasło</a></span>
			</div>
		</div>
	</div>
@endsection