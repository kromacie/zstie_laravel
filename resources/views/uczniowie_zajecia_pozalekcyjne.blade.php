@extends('layouts.subsite', ['panel' => 'includes.student'])
@section('content2')

<div class="content">
    <p>Wykaz zajęć pozalekcyjnych w roku szkolnym 2015/2016 w ZSTiE</p><hr>
    Od dnia 24 października w szkole będą prowadzone zajęcia pozalekcyjne.
    <p>Poniżej kursy, nauczyciele prowadzący oraz dni tygodnia i godziny prowadzenia zajęć.</p>
    <li>1.IT Essentials - grupa 1- prowadzący Wiktor Ziętara- wtorki godzina 1500, pracownia 119</li>
    <li>2.IT Essentials - grupa 2- prowadzący Jacek Malinowski- poniedziałek godzina 1445, pracownia 412</li>
    <li>3.CCNA Discovery - po zakończeniu kursu grupy z IT Essentials</li>
    <li>4.CCNA Routing & Switching - prowadzący Wiktor Ziętara- wtorki godzina 1500, pracownia 119</li>
    <li>5.ECDL - prowadzący Krystyna Rozdzielska- czwartki godzina 1500, pracownia 119</li>
    <li>6.C++ - prowadzący Stanisław Bajor- wtorki godzina 1500, pracownia 413</li>
    <li>7.LNU - prowadzący: Izabela Białobrzeska, Wiktor Ziętara- spotkanie organizacyjne- poniedziałek 24 października 2016 pracownia 104</li>
    <li>8.Python - prowadzący: Izabela Białobrzeska- poniedziałek godzina 1500 pracownia 104</li>
    <li>9.Mój pierwszy biznes przed 20 - opiekun Wiktor Ziętara</li>
    <li>10.Certyfikaty językowe FCE, CAE, TOEFL - prowadzący Marek Lisiecki, czwartek 7:45, sala 102</li>
</div>

@endsection
