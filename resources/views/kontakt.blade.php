@extends('layouts.subsite', ['panel' => 'includes.school'])
@section('content2')

<div id="schoolContact">
    <table>
        <tr>
            <th>Nazwa:</th>
            <td>Zespół Szkół Teleinformatycznych i Elektronicznych</td>
        </tr>
        <tr>
            <th>ulica:</th>
            <td>ul. Gen. Józefa Haukego-Bosaka 21</td>
        </tr>
        <tr>
            <th>Miejscowość:</th>
            <td>50-447 Wrocław</td>
        </tr>
        <tr>
            <th>Telefon:</th>
            <td>tel. 71 7986933</td>
        </tr>
        <tr>
            <th>fax:</th>
            <td>fax. 71 341-95-23</td>
        </tr>
        <tr>
            <th>E-mail:</th>
            <td><a href="#">szkola@zstie.edu.pl</a></td>
        </tr>
    </table>
</div>

@endsection

