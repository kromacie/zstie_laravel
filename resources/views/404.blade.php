@extends('layouts.main')
@section('content')

<div class="content" noselect center err404>
	<img src="{{ push('images/404.jpg', 'image') }}" alt="404">
	<p>Przepraszamy, wystąpił problem z serwerami. Spróbuj ponownie za chwilę :)</p>
	<hr breaker>
	<a big="true" href="{{ route('home') }}" name="fine" style="display: block">Wróć na stronę główna</a>
	<hr breaker>
</div>

@endsection