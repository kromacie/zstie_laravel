@extends('layouts.subsite', ['panel' => 'includes.student'])
@section('content2')

<div class="content">
    <p>Kalendarz imprez</p><hr>
    <b>Kalendarz uroczystości i imprez szkolnych w Zespole Szkół Teleinformatycznych i Elektronicznych w roku szkolnym 2017/2018</b>
    <p>Plik do pobrania w formacie <a href="{{ url('pdf/kalendarz_imprez_2017_2018.pdf') }}" target="_blank">PDF.</a></p></p>
</div>
<div class="content">
    <p>Klub "KLIN" we Wrocławiu</p><hr>"Zachęcamy do korzystania z kalendarza ważnych wydarzeń klubu KLIN we Wrocławiu":
    <p><a href="http://www.wcdn.wroc.pl/klin/" target="_blank">http://www.wcdn.wroc.pl/klin/</a></p>
</div>

@endsection