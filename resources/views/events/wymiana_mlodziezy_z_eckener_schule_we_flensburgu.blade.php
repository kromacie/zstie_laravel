@extends('layouts.events')
@section('content2')

<div class="content">
	<p big="true"><strong>Eckener Schule</strong></p>
	<p>ZSTiE - Eckener-Schule: Wrocław, 06.11.2017 - 09.11.2017</p>
	<p><strong>Polsko-niemiecka wymiana młodzieży ZSTiE - Eckener-Schule Wrocław, 06.09.11.2017</strong></p>
	<p>Od 6. do 9. listopada 2017 r. ponownie gościliśmy uczniów i nauczycieli z Eckener-Schule z Flensburga.</p>
	<p>Tematem przewodnim tegorocznego spotkania była "Młodzież i internet - tożsamość, zagrożenia, hakerzy, portale społecznościowe".</p>
</div>
<div class="content">
	<p><strong>Poniedziałek, 6.11</strong></p>
	<p>Nasi goście przyjechali do Wrocławia wczesnym rankiem w poniedziałek. Z tymczasowego dworca PKS odebrali ich p. Jolanta Trzebniak oraz uczniowie rodzin goszczących.</p>
	<p>Po przybyciu do budynku szkoły przy ul. Haukego-Bosaka uczniowie i nauczyciele z Flensburga zostali powitani przez Dyrektora ZSTiE - p. mgr. Rafała Cichockiego oraz zjedli tradycyjne, polskie śniadanie.</p>
	<p>O godz. 9:30 rozpoczęło się zwiedzanie szkoły. Nasi niemieccy goście mieli okazję zobaczyć m.in. wyposażenie pracowni komputerowych i kilku innych sal, biblioteki oraz halę sportową.</p>
	<p>Po zwiedzeniu szkoły uczniowie i nauczyciele udali się pod opieką p. mgr Agnieszki Idczak i p. mgr Aliny Trzeciak do Dzielnicy Czterech Świątyń, gdzie zwiedzili Synagogę pod Białym Bocianem przy ul. Włodkowica. Dla większości z nich był to pierwszy kontakt "live" z kulturą i tradycją żydowską. Poza wnętrzem synagogi grupa zwiedziła także salę nabożeństw Wrocławskiej Gminy Żydowskiej, w której obejrzała Torę w wersji oryginalnej - na skórze zawiniętej na dwie rolki, i współczesnej - w wydaniu książkowym.</p>
	<p>Po powrocie do szkoły, w sali 116 odbyły się rozgrywki online pod okiem p. mgr Mirosławy Sochalskiej.</p>
	<p>O godz. 15:30 uczniowie niemieccy zostali odebrani przez rodziców rodzin goszczących.</p>
</div>
<div class="content">
	<p><strong>Wtorek, 7.11</strong></p>
	<p>Wtorek, to już tradycyjnie dzień "wyjazdowy" podczas każdej wymiany młodzieży. W tym roku były to zamek i Stado Ogierów w Książu. Wszyscy byli pod wielkim wrażeniem tej zbudowanej z tak wielkim rozmachem budowli oraz jej długiej, skomplikowanej historii. Pan przewodnik pokazał nam nie tylko te najbardziej reprezentacyjne sale, ale także kilka pomieszczeń w zamkowych podziemiach.</p>
	<p>Przysłowiową "wisienką na torcie" była wizyta w pobliskiej stadninie... bo nie każdy ma na co dzień do czynienia z końmi i do tego pełnej krwi... ?</p>
</div>
<div class="content">
	<p><strong>Środa, 8.11</strong></p>
	<p>Trzeci dzień wymiany rozpoczął się od warsztatów w pracowni komputerowej pt. "Młodzież i internet - tożsamość, zagrożenia, hakerzy, portale społecznościowe", które poprowadziła p. mgr Mirosława Sochalska. Uczniowie polscy i niemieccy obejrzeli prezentację multimedialną, po czym przygotowali plakaty na w/w temat i zaprezentowali je swoim kolegom i nauczycielom.</p>
	<p>Po warsztatach przyszedł czas na zwiedzanie Wrocławia. Pogoda dopisała - co prawda nie było najcieplej, ale na szczęście nie padał deszcz. Podczas prawie trzygodzinnego spaceru nasi goście oraz uczniowie ich goszczący poznali historię stolicy Dolnego Śląska oraz zwiedzili m.in. Rynek, Stare Jatki, gmach główny Uniwerytetu Wrocławskiego, Wyspę Piasek i Ostrów Tumski.</p>
	<p>Środowy program wymiany zakończył się meczem siatkówki w szkolnej hali sportowej zorganizowanym przez p. mgr Alicję Kloch.</p>
</div>
<div class="content">
	<p><strong>Czwartek, 9.11</strong></p>
	<p>Ostatni dzień wymiany rozpoczął się wykładem p. dyr. Wiktora Ziętary pt. "Bezpieczeństwo w sieci LAN". Uczniowie i nauczyciele obejrzeli prezentację multimedialną oraz zapoznali się z możliwościami ochrony swoich danych w sieci.</p>
	<p>Następnym punktem programu było zwiedzanie Panoramy Racławickiej oraz Stadionu Miejskiego pod opieką p. dyr. Marka Lisieckiego. Panorama Racławicka wywarła na uczniach z Flensburga ogromne wrażenie - ogrom pracy wielu artystów-malarzy oraz historia powstania dzieła i sprowadzenie go po II Wojnie Światowej ze Lwowa do Wrocławia.</p>
	<p>Zupełnie innych (ale również samych pozytywnych) wrażeń dostarczyło wszystkim zwiedzanie Stadionu Miejskiego. Loża VIPów, szatnie piłkarzy, trybuny, areszt... Kilku uczniów pierwszy raz obejrzało obiekt sportowy takiej klasy "z bliska".</p>
	<p>Czwartkowe popołudnie uczniowie z Flensburga spędzili u rodzin ich goszczących, a o godz. 21:30 udali się z nowego dworca autobusowego w CH Wroclavia w drogę powrotną do domu.</p>
	<p>W spotkaniu szkół partnerskich ZSTiE/ Wrocław - Eckener-Schule/ Flensburg wzięli udział następujący uczniowie:
	<li>Łukasz Bieliński & Mike Hamdorf</li>
	<li>Mateusz Diduszko & David Szewc</li>
	<li>Eryk Prystasz & Kion Jonas Horn</li>
	<li>Tomasz Maj & Tjalf Petry</li>
	<li>Michal Choma</li>
	<li>Konrad Borkowicz & Felix Kolodziej</li>
	<li>Oktawian Lupa & Jule Johannsen</li>
	<li>Wojciech Nabiałkowski & Helene Sischka</li>
	<li>Oskar Kikut & Bente Andresen</li>
	<li>Michał Nowak & Niklas Gorka</li>
	<li>Maksymilian Słodyński & Marvin Hewett.</li>
	</p>
	<p>W organizację całego przedsięwzięcia zaangażowani byli następujący nauczyciele:</p>
	<span><strong>ECKENER-SCHULE</strong>:</span>
	<li>p. Edyta Kruczynski - koordynatorka</li>
	<li>p. Sarah Julius</li>
	<li>p. Georg Reuters</li>
	<span><strong>ZSTiE</strong>:</span>
	<li>p. Agnieszka Idczak</li>
	<li>p. Jolanta Trzebniak</li>
	<li>p. Mirosława Sochalska</li>
	<li>p. Alina Trzeciak</li>
	<li>p. Agnieszka Skrzypaszek</li>
	<li>p. Alicja Kloch</li>
	<li>p. dyr. Wiktor Ziętara</li>
	<li>p. dyr. Marek Lisiecki</li>
	<p>Wszystkim uczniom i nauczycielom dziękujemy za udział i zaangażowanie!</p>
	<p>Więcej zdjęć z wymiany znajdziecie w naszej <a href="#">Galerii</a></p>
	<p>Wymianę opisała p. Agnieszka Idczak</p>
</div>

@endsection