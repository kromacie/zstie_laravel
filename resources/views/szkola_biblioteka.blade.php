@extends('layouts.subsite', ['panel' => 'includes.school'])
@section('content2')

<div class="content">
    <p>O nas</p><hr>
Zapraszamy czytelników do korzystania z biblioteki i czytelni od poniedziałku do piątku w godzinach 7.30- 15.00 Czytelnia pełni funkcję pracowni multimedialnej, uczniowie mają do dyspozycji osiem stanowisk komputerowych, drukarkę laserową i skaner. Od lat współpracujemy z Mediateką i Dolnośląską Biblioteką Pedagogiczną.
</div>
<div class="content">
    <p>Misja</p><hr>
Misją Biblioteki jest wspieranie procesu uczenia się i nauczania, zaspo­kajanie potrzeb edukacyjnych i czytelniczych uczniów oraz rozwijanie ich zainteresowań poprzez dostarczanie źródeł informacji. Biblioteka służy również nauczycielom w pracy dydaktyczno-wychowawczej, wspiera ich dokształcanie i doskonalenie zawodowe.<br> Bibliotekarzom szczególnie zależy na stwarzaniu możliwości wykorzys­tania najnowszych technologii informacyjnych zarówno w pracy dydak­tycznej, jak i w indywidualnym poszerzaniu wiedzy przez uczniów. Dlatego ogromne znaczenie przypisujemy pracowni multimedialnej. Biblioteka poszerza swoje zbiory, opracowuje je i udostępnia uczniom, nauczycielom i wszystkim pracownikom szkoły.
</div>
<div class="content">
    <p>Regulamin</p><hr>
    <li>Z biblioteki szkolnej mogą korzystać: uczniowie, nauczyciele, inni pracownicy szkoły i rodzice.</li>
    <li>Uprawnieni czytelnicy Wrocławskiego Systemu Bibliotecznego (WSB ) spoza ZSTiE mogą korzystać ze zbiorów biblioteki wyłącznie na miejscu.</li>
    <li>Biblioteka jest czynna od poniedziałku do piątku w ustalonych i podanych do wiadomości czytelników godzinach.</li>
    <li>Uczniowie mogą wypożyczyć maksymalnie 2 lektury na miesiąc.</li>
    <li>Uczniowie mogą mieć na koncie maksymalnie 5 książek.</li>
    <li>Czytelnicy zobowiązani są do rozliczenia się z biblioteką:</li>
    <ul>- w terminie wyznaczonym przez nauczyciela bibliotekarza</ul>
    <ul>- mogą przedłużyć termin zwrotu wypożyczonej książki o 2 tygodnie</ul>
    <ul>- o dłuższym terminie przedłużenia decyduje nauczyciel bibliotekarz</ul>
</div>
<div class="content">
    <p>W przypadku nieoddania wypożyczonych dokumentów w określonym terminie WSB blokuje konto czytelnika.</p><hr>
    <li>Książkę zgubioną lub zniszczoną czytelnik powinien odkupić (ten sam tytuł lub inny uzgodniony z bibliotekarzem).</li>
    <li>Najlepsi czytelnicy w danej klasie pod koniec semestru otrzymują + 15 punktów z zachowania.</li>
    <li>Z księgozbioru podręcznego czytelnik może korzystać wyłącznie na miejscu (w czytelni lub na lekcji) po pozostawieniu u nauczyciela bibliotekarza legitymacji szkolnej lub numerka z szatni.</li>
    <li>Czytelnik ma prawo do fachowej informacji na temat zbiorów bibliotecznych.</li>
    <li>Czytelnicy odbierający dokumenty lub kończący szkołę są zobowiązani do zwrotu wypożyczonych dokumentów i uzyskania potwierdzenia przez nauczyciela bibliotekarza o zwrocie tych dokumentów.</li>
    <li>Każdy użytkownik biblioteki ma obowiązek zapoznania się z regulaminem biblioteki i przestrzegania zawartych w nim przepisów.</li>
</div>
<div class="content">
    <p>OD CHWILI WYPOŻYCZENIA CZYTELNIK ODPOWIADA ZA KSIĄŻKĘ.</p><hr>Wrocław, 1 IX 2016 r.
</div>
<div class="content">
    <p>Zasady korzystania</p><hr>
    <li>Pracownia multimedialna stanowi część czytelni, jest udostępniana uczniom, nauczycielom oraz innym pracownikom szkoły w godzinach pracy biblioteki.</li>
    <li>Osoby korzystające z pracowni wpisują do zeszytu odwiedzin swoje imię, nazwisko, klasę, godziny pracy, nr komputera.</li>
    <li>Dostrzeżone usterki przy stanowisku pracy należy zgłosić bibliotekarzowi.</li>
    <li>Użytkownicy indywidualni korzystają z komputera do 1 godziny.</li>
    <li>Można przedłużyć czas pracy przy komputerze za zgodą nauczyciela.</li>
    <li>Przy stanowisku komputerowym może pracować 1 osoba, o ile nauczyciel nie zdecyduje inaczej.</li>
    <li>Korzystanie z Internetu, multimediów i programów użytkowych ma służyć wyłącznie celom naukowym, informacyjnym i edukacyjnym.</li>
    <li>Użytkownik ma prawo wykorzystać:</li>
    <ul>- dane i programy udostępniane w systemie komputerowym szkoły.</ul>
    <ul>- urządzenia komputerowe: drukarkę, skaner znajdujące się w pracowni.</ul>
    <ul>- własne dane pobrane z Internetu lub przyniesione na wymiennych nośnikach.</ul>
    <li>Użytkownik ma prawo do wydrukowania własnych dokumentów zawierających treści związane z programami nauczania obowiązującymi w szkole.
<b>Użytkownik jest zobowiązany do przyniesienia własnego papieru</b>.</li>
    <li>Pierwszeństwo do korzystania z pracowni mają:</li>
    <ul>- klasy podczas prowadzonych lekcji</ul>
    <ul>- osoby realizujące konkretny temat</ul>
    <li>Zabrania się:</li>
    <ul>- dokonywania zmian w oprogramowaniu.</ul>
    <ul>- instalowania i wgrywania programów zewnętrznych.</ul>
    <ul>- wykorzystania stanowiska komputerowego w celach zarobkowych.</ul>
    <ul>- łamania praw autorskich.</ul>
    <li>Użytkownik ponosi materialną odpowiedzialność za straty lub koszt naprawy w przypadku zniszczeń i szkód wynikających z niewłaściwego użytkowania sprzętu. W przypadku ucznia niepełnoletniego odpowiedzialność materialną ponoszą rodzice.</li>
    <li>Przy stanowisku nie wolno spożywać posiłków i napojów.</li>
    <li>W przypadku nieprzestrzegania regulaminu bibliotekarz ma prawo przerwać pracę użytkownika przy komputerze.</li>
</div>

@endsection