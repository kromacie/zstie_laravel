@extends('layouts.main')
@section('content')
	<noscript><div class="content" global warrContent><div><span>Uwaga, wykryliśmy brak obsługi javascript w Twojej przeglądarce. Strona ZSTiE nie będzie działać poprawnie bez jego aktywacji. Instrukcje jak włączyć javascript w różnych przeglądarkach znajdziesz tutaj: <a href="https://enable-javascript.com/pl/" target="_blank">https://enable-javascript.com/pl/</a></span></div></div></noscript>
	<div id="newsAdder">
		<a href="{{ url('/404') }}">błąd 404 (<span name="att">TYMCZASOWE</span>)</a>
		@can('publish articles')
			<a href="{{ route('news.unpublished.show') }}">Kolejka artykułów (<span name="att">Tylko dla administracji! TYMCZASOWE</span>) </a>
		@endcan
		@can('create articles')
			<a href="{{ route('news.create') }}">Dodaj artykuł</a>
		@endcan
	</div>

	@include('includes.news')

	<div id="addonsContainer">
		<div id="calendar">
			<p style="font-size: 1.4rem;">Kalendarz wydarzeń</p>
			@foreach($events as $event)
			<a href="{{ route('news.show', ['id' => $event->id]) }}" class="calendarEvent">
				<span class="date">{{ $event->calendar_date->format('d/m') }}</span>
				<span class="description">{{ $event->description }}</span>
			</a>
			@endforeach
		</div>
		<div id="map">
			<p><a href="{{ url('map') }}">Tutaj nas znajdziesz</a></p>
			<div>
				<img src="{{ push('images/aside/map.png', 'image') }}" alt="mapa">
				<a href="{{ url('map') }}" class="interHelper">Przejdź do interaktywnej mapy</a>
			</div>
		</div>
	</div>
	<div class="content" global>
		<a href="https://visitwroclaw.eu/wroclaw-dla-rodzica-od-a-do-z" target="_blank"><img src="{{ push('images/_other/wroclawdla-rodzic-1.jpg', 'image') }}" alt=""></a>
	</div>
	<div class="content" global>
		<div><a href="">Przykładowy link</a></div>
		<p><a href="">Przykładowy link</a></p>
		<p><span>[SPAN W P] Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit culpa odio fuga ratione tempora assumenda labore magnam, autem quo. Laborum cum dolorem quaerat quos eligendi optio ratione distinctio porro in debitis, praesentium voluptate culpa consequuntur. Placeat quisquam hic inventore veniam velit error libero, est labore dolorem quis explicabo maiores commodi voluptatum ullam quia aspernatur aperiam minus deserunt, odio. Hic eaque fugit, unde necessitatibus doloribus ex maiores perferendis reprehenderit expedita illo ratione, magni optio. At, non ex? Ipsa nobis omnis corporis pariatur accusamus, iure, labore in aspernatur quia tenetur totam illo voluptatum assumenda tempore beatae placeat. Culpa minus accusantium libero commodi?</span></p>
		<div><span>[SPAN W DIVIE] Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque nobis dolore voluptatem, deleniti ipsa quidem nesciunt et fugit mollitia excepturi, at quos aliquid ratione. Totam nobis nam distinctio odio, pariatur repudiandae ipsum quae dignissimos quia eveniet quibusdam earum, dolore inventore magni, praesentium debitis sequi omnis a expedita! Mollitia minus, excepturi veritatis illo porro possimus delectus qui distinctio, iste ad dolore? Modi laudantium minima necessitatibus blanditiis, rerum, tempore neque harum quae sunt asperiores sed voluptatibus iste reiciendis. Ipsa officia aperiam explicabo qui aliquam incidunt, distinctio vero possimus dicta est ipsam earum odio provident perferendis, repudiandae, cum doloribus totam eligendi quisquam esse?</span></div>
		<span>[SAM SPAN] Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio eos dolore explicabo illum quo itaque, nulla excepturi suscipit numquam officiis provident animi, facere a qui quod atque obcaecati, iusto saepe sequi necessitatibus dolor magni harum! Quidem, ducimus ipsam, ullam, est atque, qui obcaecati itaque quasi culpa reiciendis explicabo. Deleniti modi hic accusantium perferendis tempora id aperiam, soluta sequi provident inventore dignissimos molestias perspiciatis quasi, debitis vel fugiat numquam dolore minima delectus expedita? Inventore quas at corporis, necessitatibus nostrum voluptatibus exercitationem rerum dolorum deserunt quos cum magnam ratione doloribus commodi porro minima asperiores blanditiis eum perspiciatis recusandae quam, unde! Cumque, voluptatem!</span>
		<a href="">sam link</a>
		<li>samo li</li>
		<ol>
			sama lista numerowana
			<li><a href="">produkt 1</a></li>
			<li>produkt 2</li>
		</ol>
		<ul>
			sama lista nienumerowana
			<li><a href="">produkt 1</a></li>
			<li>produkt 2</li>
		</ul>
		<div>
			<ol>
				lista numerowana w divie
				<li><a href="">produkt 1</a></li>
				<li>produkt 2</li>
			</ol>
			<ul>
				lista nienumerowana w divie
				<li><a href="">produkt 1</a></li>
				<li>produkt 2</li>
			</ul>
		</div>
	</div>
	<div class="content" global>
		<div><span>You know nothing Jon Snow.</span></div>
		<div><span>You shall not pass!</span></div>
		<div><span>May I kill you?</span></div>
	</div>
	<div id="collabs">
		<p style="font-size: 1.4rem;">Nasi partnerzy</p>
		<div class="collabContainer">
			<div class="collab">
				<a href="#"><img src="{{ push('images/socials/wsb.png', 'image') }}" alt="wyższa szkoła bankowa"></a>
			</div>
			<div class="collab">
				<a href="#"><img src="{{ push('images/socials/ibm.png', 'image') }}" alt="ibm"></a>
			</div>
			<div class="collab">
				<a href="#"><img src="{{ push('images/socials/samorzad.jpg', 'image') }}" alt="samorząd"></a>
			</div>
			<div class="collab">
				<a href="#"><img src="{{ push('images/socials/pryzmat_b.jpg', 'image') }}" alt="pracownia fizyki"></a>
			</div>
			<div class="collab">
				<a href="#"><img src="{{ push('images/socials/doradztwo.jpg', 'image') }}" alt="doradztwo zawodowe"></a>
			</div>
			<div class="collab">
				<a href="#"><img src="{{ push('images/socials/akademia_cisco2.png', 'image') }}" alt="akademia cisco"></a>
			</div>
			<div class="collab">
				<a href="#"><img src="{{ push('images/socials/robotyczny.png', 'image') }}" alt="klub robotyczny"></a>
			</div>
			<div class="collab">
				<a href="#"><img src="{{ push('images/socials/bip_logo_2.jpg', 'image') }}" alt="biuletyn informacji publicznej"></a>
			</div>
		</div>
	</div>
@endsection
