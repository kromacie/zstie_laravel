@extends('layouts.main')
@section('content')
<div class="content">
	<form action="{{ route('news.delete.process') }}" method="post">
		@csrf
		<p>Czy na pewno chcesz usunąć newsa o tytule: {{$news->title}}? </p>
		<input type="hidden" name="id" value="{{ $news->id }}">
		<input type="submit">
	</form>
</div>
@endsection