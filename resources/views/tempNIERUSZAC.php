<div class="content">
    <p>Poprzednie wydarzenia</p><hr>
    <p>Sportowcy z ZSTiE</p>
    Uczeń klasy 4C, Konrad Poprawa znalazł się w 18 meczowej w wygranym 2:1 meczu WKS Śląska Wrocław - Legia Warszawa w Lotto Ekstraklasie.
    <p><a href="http://slaskwroclaw.pl/strona/aktualnosci/sklad-slaska-na-legie-12841903" target="_blank">http://slaskwroclaw.pl/strona/aktualnosci/sklad-slaska-na-legie-12841903</a></p>
    </div>
    <div class="content">
    <p>Projekt IPN „Niezłomni – Niepochowani” - uczczenie ofiar UB</p><hr>
    Adresatem projektu byli uczniowie naszej szkoły, którzy z zainteresowaniem brali udział w różnych formach propagowania wiedzy z historii najnowszej. Na terenie szkoły i z inicjatywy nauczycieli:
    <li><b>J. Trzebniak</b></li>
    <li><b>B. Kręgielewskiej</b></li>
    <li><b>H. Strachowskiej</b></li>
    <li><b>B. Hołowni</b></li>
    miały miejsce działania poprzedzające udział w projekcie. We wrześniu i październiku 2016r. zorganizowano wystawę pod hasłem: „Akcja Burza”. Uczniowie oddali cześć Żołnierzom Niezłomnym biorąc udział w uroczystościach na Cmentarzu Osobowickim 1 Marca w Narodowym Dniu Pamięci Żołnierzy Wyklętych.
    Kolejnym działaniem był wykład poświęcony największej operacji UB Lawina, który prowadził Naczelnik Wydziału Opolskiego IPN dr Bartosz Kuświk. Wypowiedzi wysłuchali uczniowie klas 1c i 3b wraz z nauczycielami.<br>
    Interesującym przedsięwzięciem była gra szkolna „Bartek” wg scenariusza przygotowanego przez IPN, który można było modyfikować. Uczestniczyło w niej 5 drużyn składających się z uczniów klas 1-3. Była to ciekawa lekcja historii, uczniowie nie tylko wyszukiwali informacje, ale także łączyli je w ciągi tematyczno-historyczne. Wydarzeniem podsumowującym ten etap był udział w gali w opolskim Ratuszu z udziałem Oddziału Opolskiego IPN, na której wręczono szkołom, biorącym udział w projekcie, certyfikaty.<br> 
    Zamknięciem udziału w projekcie był wyjazd 14 czerwca 2017r. uczniów klas trzecich do Starego Grodkowa na leśnie mogiły pomordowanych żołnierzy z Oddziału Henryka Flame ps. „Bartek”. Odbyło się spotkanie z pracownikami realizującymi projekt „Poszukiwanie miejsc pochówków…”, i wysłuchali wykład na ten temat. Najważniejszym przeżyciem dla uczniów był moment refleksji towarzyszący zapaleniu zniczy w barwach biało-czerwonych pod Krzyżem Pamięci o pomordowanych, niewinnych ofiarach systemu komunistycznego w Polsce. Projekt cieszył się dużym zainteresowaniem, na prośbę uczniów będzie powtórzony w roku następnym.
    <p class="events_gallery">
        <a href="<?=RS?>img/gallery/Wycieczka_grodkow/1.jpg"><img src="<?=RS?>img/gallery/Wycieczka_grodkow/1.jpg"></a>
        <a href="<?=RS?>img/gallery/Wycieczka_grodkow/2.jpg"><img src="<?=RS?>img/gallery/Wycieczka_grodkow/2.jpg"></a>
        <a href="<?=RS?>img/gallery/Wycieczka_grodkow/3.jpg"><img src="<?=RS?>img/gallery/Wycieczka_grodkow/3.jpg"></a>
        <a href="<?=RS?>img/gallery/Wycieczka_grodkow/4.jpg"><img src="<?=RS?>img/gallery/Wycieczka_grodkow/4.jpg"></a>
        <a href="<?=RS?>img/gallery/Wycieczka_grodkow/5.jpg"><img src="<?=RS?>img/gallery/Wycieczka_grodkow/5.jpg"></a>
        <a href="<?=RS?>img/gallery/Wycieczka_grodkow/6.jpg"><img src="<?=RS?>img/gallery/Wycieczka_grodkow/6.jpg"></a>
        <a href="<?=RS?>img/gallery/Wycieczka_grodkow/7.jpg"><img src="<?=RS?>img/gallery/Wycieczka_grodkow/7.jpg"></a>
    </p>
</div>