@extends('layouts.main')

@section('content')

<div class="behind">
	<div class="content">
		<p class="small-caps" name="fine" big="true">polityka cookies</p>
		<p>Zgodnie z ustawą "Prawo komunikacyjne" (Dziennik Ustaw z 21.12.2012, poz. 1445) która weszła w życie 22 marca 2013 roku informujemy, iż serwis ZSTiE korzysta z ciasteczek. Korzystając z witryny bez zmian domyślnych przeglądarek oznacza, że będą one umieszczane w Twoim urządzeniu końcowym.</p>
	</div>
	<div class="content">
		<p class="small-caps" medium="true">krótko o ciasteczkach</p>
		<p>Ciasteczka na niniejszej stronie służą do przechowywania informacji na temat ustawień strony, nie pobierają danych od użytkowników, a ich istnienie sprawia, że użytkowanie staje się przyjemniejsze.</p>
		<p>Więcej informacji na temat ciasteczek możesz się dowiedzieć tutaj: <a href="http://wszystkoociasteczkach.pl/polityka-cookies/" target="_blank">http://wszystkoociasteczkach.pl/polityka-cookies/</a></p>
	</div>
</div>

@endsection