@extends('layouts.subsite', ['panel' => 'includes.school'])
@section('content2')

<div class="content">
    <p>Ubezpieczenie na rok szkolny 2017/2018</p><hr>
Informacje na temat warunków ubezpieczenia uczniów w roku szkolnym 2017/2018:
    <p>polisa NNW dla ZSTiE - plik w formacie <a href="{{ url('pdf/polisa_nnw.pdf') }}" target="_blank">PDF</a></p>
    <p>OWU NNW do polisy - plik w formacie <a href="{{ url('pdf/owu_nnw.pdf') }}" target="_blank">PDF</a></p>
    <p>aneks nr 1 do OWU NNW - plik w formacie <a href="{{ url('pdf/aneks1_owu.pdf') }}" target="_blank">PDF</a></p>
    <p>tabela oceny procentowej trwałego uszczerbku na zdrowiu - plik w formacie <a href="{{ url('pdf/tabela_uszczerbek.pdf') }}" target="_blank">PDF</a></p>
    <p>aneks nr 1 do tabeli - plik w formacie <a href="{{ url('pdf/aneks1_uszczerbek.pdf') }}" target="_blank">PDF</a></p>
    <p>zakres ubezpieczenia grupowego (w tabeli) - plik w formacie <a href="{{ url('pdf/skrocony_opis.pdf') }}" target="_blank">PDF</a></p>
</div>
<div class="content">
    <p>Spotkania z rodzicami 2017/2018</p><hr>
Harmonogram spotkań z rodzicami w roku szkolnym 2017/2018.
    <p><a href="{{ url('pdf/spotkania_rodzice_2017.pdf') }}" target="_blank">Plik w formacie PDF</a></p>
</div>
<div class="content">
    <p>Protokół nr 4 RR</p><hr>
Protokół nr 4 z posiedzenia Rady Rodziców przy ZSTiE we Wrocławiu z dnia 30 marca 2017 r.
    <p>Dokument do <a href="{{ url('pdf/protokol_04_30_03_2016_2017.pdf') }}" target="_blank">pobrania</a> w formacie PDF.</p>
</div>
<div class="content">
    <p>Protokół nr 3 RR</p><hr>
Protokół nr 3 z posiedzenia Rady Rodziców przy ZSTiE we Wrocławiu z dnia 19 grudnia 2016 r.
    <p>Dokument do <a href="{{ url('pdf/protokol_03_19_12_2016_2017.pdf') }}" target="_blank">pobrania</a> w formacie PDF.</p>
</div>
<div class="content">
    <p>Protokół nr 2 RR</p><hr>
Protokół nr 2 z posiedzenia Rady Rodziców przy ZSTiE we Wrocławiu z dnia 16 listopada 2016 r.
    <p>Dokument do <a href="{{ url('pdf/protokol_02_16_11_2016_2017.pdf') }}" target="_blank">pobrania</a> w formacie PDF.</p>
</div>
<div class="content">
    <p>Protokół nr 1 RR</p><hr>
Protokół nr 1 z posiedzenia Rady Rodziców przy ZSTiE we Wrocławiu z dnia 28 września 2016 r.
    <p>Dokument do <a href="{{ url('pdf/protokol_01_28_09_2016_2017.pdf') }}" target="_blank">pobrania</a> w formacie PDF.</p>
</div>
<div class="content">
    <p>Rada Rodziców na rok 2016/2017</p><hr>
Na podstawie Regulaminu Rady Rodziców przy ZSTiE we Wrocławiu (§ 5 ust. 1 i ust.2), wybrano Prezydium Rady Rodziców składającego się z siedmiu osób:
    <p>1. Magdalena Lazarek ? Litwin (3T) jako Przewodnicząca Rady Rodziców Aneta Starczyńska (4C) jako Zastępca Przewodniczącej Rady Rodziców.</p>
    <p>2. Renata Kołosowska (4A) jako Skarbnik Rady Rodziców.</p>
    <p>3. Iwona Słodyńska (2C) jako Sekretarz Rady Rodziców.</p>
    <p>4. Leszek Cieśla (3B) jako członek Prezydium Rady Rodziców.</p>
    <p>5. Marek Nabiałkowski (2B) jako członek Prezydium Rady Rodziców.</p>
    <p>6. Agnieszka Główka (1B) jako członek Prezydium Rady Rodziców.</p>
</div>
<div class="content">
    <p>Uchwała RR - Prezydium RR na rok szk. 2016/2017</p><hr>
Uchwała Rady Rodziców z dnia 28 września 2016 r. w sprawie wyboru Prezydium Rady Rodziców na rok szkolny 2016/2017.
    <p>Dokument do <a href="{{ url('pdf/uchwala_01_28_09_2016_2017.pdf') }}" target="_blank">pobrania</a> w formacie PDF.</p>
</div>
<div class="content">
    <p>Konto bankowe RR</p><hr>
Informacja o numerze konta bankowego Rady Rodziców przy ZSTiE we Wrocławiu.
    <p>Konto w Banku Pocztowym S.A.: 60 1320 1999 2340 8962 2000 0001</p>
</div>
<div class="content">
    <p>Informacja o kontach w ZSZWO</p><hr>
Konta dla uczniów, rodziców (opiekunów prawnych) w Zintegrowanym Systemie Zarządzania Wrocławską Oświatą <br>
W ramach projektu Zintegrowanego Systemu Zarządzania Wrocławską Oświatą, oddany został portal oświatowy www.edu.wroclaw.pl ,dzięki któremu można sprawdzić najważniejsze wydarzenia w kalendarzu wrocławskiej edukacji. Pełna funkcjonalność kalendarza dostępna jest dla osób posiadających konto na portalu Wrocławska Edukacja <a href="http://www.edu.wroclaw.pl" target="_blank">www.edu.wroclaw.pl</a>. <br>
Zachęcamy do utworzenia konta, dzięki któremu będzie dostęp nie tylko do kalendarza, ale również do dziennika elektronicznego, platformy edukacyjnej, konta w chmurze Microsoft (poczta, dysk sieciowy Office Online). Można również sprawdzić dostępność książki w bibliotece. Aby utworzyć konto na portalu, należy zgłosić się sekretariatu szkoły lub samemu wydrukować odpowiedni wniosek ze strony <a href="http://www.zstie.edu.pl" target="_blank">www.zstie.edu.pl</a>.
</div>
<div class="content">
    <p>Wzory wniosków w załączniku:</p><hr>
    <li>WUK – wniosek o utworzenie konta ucznia na portalu internetowym „Wrocławska Edukacja” oraz wydanie i personalizacja elektronicznej karty</li>
    <li>WU – wniosek o utworzenie konta ucznia na portalu internetowym „Wrocławska Edukacja”</li>
    <li>WR – wniosek o utworzenie konta rodzica (opiekuna prawnego) na portalu internetowym „Wrocławska Edukacja”</li>
    <li>WK – wniosek o wydanie i personalizację elektronicznej karty identyfikacyjnej</li>
    <li>WKR – wniosek o rejestrację numeru elektronicznej karty identyfikacyjnej</li>
</div>
<div class="content">
    <p>Regulamin Rady Rodziców</p><hr>
Podstawa prawna współdziałania rodzice-szkoła. Jest to załącznik nr 3 do Statutu Szkoły.
<a href="{{ url('pdf/reg_rodzicow_2017.pdf') }}" target="_blank">Plik w formacie PDF.</a></p>
</div>
<div class="content">
    <p>Nowy numer telefonu</p><hr>
Informujemy, że od 1 stycznia 2015 r. podstawowym numerem telefonicznym szkoły jest: 71 7986933. Stare numery telefonów będą stopniowo wycofywane.
</div>
<div class="content">
    <p>Adres e-mail RR</p><hr>
Adres <a href="mailto:rada.rodzicow@zstie.edu.pl">rada.rodzicow@zstie.edu.pl</a> jest powiązana z kontami wszystkich członków Rady Rodziców; <br>natomiast adres <a href="mailto:przewodniczacy.rady.rodzicow@zstie.edu.pl">przewodniczacy.rady.rodzicow@zstie.edu.pl</a> jest dla spraw wymagających zachowania poufności danych i jest odbierana wyłącznie przez przewodniczącą RR - p. Krzesisławę Górzną.
</div>

@endsection