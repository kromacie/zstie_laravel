@extends('layouts.subsite', ['panel' => 'includes.recruitment'])
@section('content2')
<div class="content">
	<p>Skierowania lekarskie</p><hr>
	<p>Zgodnie z <a href="https://www.kuratorium.wroclaw.pl/wp-content/uploads/2017/04/zarzadzenie.pdf" target="_blank">Zarządzeniem nr 12/2017</a> DKO z dnia 28 marca 2017 r. w sprawie harmonogramu czynności w postępowaniu rekrutacyjnym oraz postępowaniu uzupełniającym do szkoły ponadgimnazjalnej na rok szkolny 2017/2018, zaświadczenie lekarskie zawierające orzeczenie o braku przeciwwskazań zdrowotnych do podjęcia praktycznej nauki zawodu, jest dokumentem niezbędnym do przyjęcia do technikum i branżowej szkoły zawodowej. Skierowania do przychodni medycyny pracy można pobierać w sekretariacie szkoły, w momencie zakwalifikowania kandydata do kształcenia w danym zawodzie.<br>Lista przychodni, w których można wykonać bezpłatnie badania uczniów w 2017 r. <br><a href="{{ url('pdf/lista_przychodni_2017.pdf') }}" target="_blank">Plik</a> do pobrania w formacie PDF.</p>
</div>
@endsection