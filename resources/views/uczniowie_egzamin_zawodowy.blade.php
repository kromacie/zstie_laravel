@extends('layouts.subsite', ['panel' => 'includes.student'])
@section('content2')


<div class="content">
    <p>Egzamin zawodowy</p><hr>
    <li><a href="http://egzamin-zawodowy.edu.pl/" target="_blank">Trenażer umiejętności zdania egzaminu zawodowego</a></li>
    <li><a href="http://egzamin-informatyk.pl/e12-e13-e14-testy-online-egzamin-zawodowy" target="_blank">Testy egzaminacyjne E12, E13, E14</a></li>
</div>
<div class="content">
    <p>Wyniki egzaminów</p><hr>
    <p>Egzamin zawodowy 2016</p><hr>Wyniki egzaminu zawodowego w sesji letniej 2016 r. w pliku <a href="{{ url('pdf/egz_zaw_lato_2016.pdf') }}" target="_blank">PDF</a>

    <p>Egzamin zawodowy 2015</p><hr>Zdawalność egzaminu potwierdzającego kwalifikacje w zawodzie w 2015 - technikum:<br>
    <li>kwalifikacja E.05: 40%; średnia dla Wrocławia - 40%; średnia dla województwa - 40%</li>
    <li>kwalifikacja E.09: 72,7%; średnia dla Wrocławia - 72,7%; średnia dla województwa - 72,7%</li>
    <li>kwalifikacja E.12: 89,6%; średnia dla Wrocławia - 87,6%; średnia dla województwa - 84%</li>
    <li>kwalifikacja E.15: 50%; średnia dla Wrocławia - 50%; średnia dla województwa - 50%</li>

    <p>Egzamin zawodowy 2014</p><hr>Zdawalność egzaminu potwierdzającego kwalifikacje zawodowe w 2014:<br><br>technikum:<br>
    <li>technik informatyk: 75%; średnia dla województwa - 51,3%</li>
    <li>technik teleinformatyk: 51,4%; średnia dla województwa - 30,8%</li>
    <li>technik telekomunikacji: 50%; średnia dla województwa - 45,8%</li>
    <br>zasadnicza szkoła zawodowa:<br>
    <li>monter elektronik: 90,9%; średnia dla województwa - 76,9%</li>
    <li>monter sieci i urządzeń telekomunikacyjnych: 53,8%; średnia dla województwa - 41,7%</li>

    <p>Egzamin zawodowy 2013</p><hr>Zdawalność egzaminu potwierdzającego kwalifikacje zawodowe w 2013:<br><br>technikum:<br>
    <li>technik informatyk: 66,7%; średnia dla województwa - 51,1%</li>
    <li>technik teleinformatyk: 37,7%; średnia dla województwa - 32,1%</li>
    <li>technik telekomunikacji: 27,6%; średnia dla województwa - 27,1%</li>
    <br>zasadnicza szkoła zawodowa:<br>
    <li>monter elektronik: 90,9%; średnia dla województwa - 85,2%</li>
    <li>monter sieci i urządzeń telekomunikacyjnych: 41,7%; średnia dla województwa - 41,7%</li>

    <p>Egzamin zawodowy 2012</p> <hr>
    Zdawalność egzaminu potwierdzającego kwalifikacje zawodowe w 2012 roku:<br>
    <li>technikum: 25,4%</li>
    <li>Zas. Szk. Zaw.: 72,5%</li>
</div>

@endsection