@extends('layouts.main')

@section('content')

<div class="behind">
	<div class="content">
		<p class="small-caps" big="true">Twórcy witryny</p><hr>
	</div>
	<div class="content">
		<p medium="true">[<span noselect class="small-caps" name="fine">frontend</span>] <span><strong><i>Daniel Fenikstfb Grodzicki</i></strong></span>
		[ <span noselect class="small-caps" name="fine">frontendowiec</span>
		|
		<span noselect class="small-caps" name="fine">pasjonata</span>
		|
		<span noselect class="small-caps" name="fine">twórca</span>
		|
		<span noselect class="small-caps" name="fine">projektant</span> ]</p>
		<p class="small-caps">Pisanie kodu zarówno pod postacią opisową jak i skryptową sprawia mi wiele radości. Nie bez przyczyny mogę nazwać szeroko rozumianego programowania moją pasją która pochłania większość mojego wolnego czasu</p>
		<p class="small-caps">Przygodę z programowaniem rozpocząłem w <i>2016</i> roku i trwa ona do dziś. Rozwijam się w produkcji stron internetowych po stronie klienta z naciskiem na <span noselect name="fine" title="UX (ang. doświadczenie użytkownika) – całość wrażeń, jakich doświadcza użytkownik podczas korzystania z produktu interaktywnego">UX</span>(User Experience)</p>
		<p class="small-caps">Poza projektowaniem i wdrażaniem stron WWW fascynują mnie gry oraz grafika komputerowa, swojego czasu zaprojektowałem cztery gry na platformie <span noselect title="RPG Maker (jap. RPGツクール RPG Tsukūru?) – program do tworzenia komputerowych gier fabularnych bez znajomości języków programowania" name="fine">RPG MAKER XP</span> i opublikowałem na forach o takowym zagadnieniu</p>
		<p class="small-caps">Moją wizją zawodową jest projektowanie pełnoprawnych aplikacji desktopowych oraz mobilnych przy użyciu środowiska <span name="fine" noselect title="środowisko uruchomieniowe zaprojektowane do tworzenia wysoce skalowalnych aplikacji internetowych, szczególnie serwerów www napisanych w języku JavaScript. Umożliwia tworzenie aplikacji sterowanych zdarzeniami wykorzystujących asynchroniczny system wejścia-wyjścia">node.js</span> oraz bibliotek <span noselect name="fine" title="język programowania po stronie klienta. Przed powstaniem node.js używany głównie w przeglądarkach internetowych">javascriptu</span></p>
		<p class="small-caps">Zachęcam do odwiedzin mojej <a href="#">strony</a> równoznaczej z wirtualnym warsztatem (portfolio), gdzie umieszczam swoje prace - <span name="fine" noselect title="czyt. strony główne">Szablony stron WWW</span>, <span noselect name="fine" title="Przeważnie pełnoprawne szablony z całą przykładową zawartością">Gotowe "większe" projekty</span>, <span name="fine">pozostałe produkcje </span>(np. <span noselect title="gry z różnych środowisk: RPG MAKER XP, Unity, etc" name="fine">gry</span>, <span noselect title="Jeżeli takowe się pojawią :P" name="fine">grafiki</span>, <span noselect title="bardzo szanuje stylistykę pixelart - jego kreacja opiera się o proste zasady, a ogranicza nas jedynie wyobraźnia, polecam!" name="fine">pixelart</span>, etc)</p>
		<p>I TUTAJ ZDJĘCIE LOGO MOJEJ STRONY ALE NAJPIERW ZROBIĘ, UWIELBIAM CIĘ EWELINA < 3 </p>
	</div>
	<div class="content">
		<p class="small-caps" big="true" name="att">Współpraca</p>
		<p>W sprawie współpracy zachęcam do kontaktu na email: <a href="mailto:kontakt.fenikstfb@gmail.com">kontakt.fenikstfb@gmail.com</a></p>
	</div><br>

	<div class="content">
		<p medium="true"><strong>[<span noselect class="small-caps" name="fine">backend</span>] Maciej Król</strong>
		[ <span noselect class="small-caps" name="fine">backendowiec</span>
		|
		<span noselect class="small-caps" name="fine">pasjonata</span>
		|
		<span noselect class="small-caps" name="fine">psychopata</span> ]</p>
		<p class="small-caps">Perfekcjonizm, zawsze. Oraz programista w Laravel 5.6, Node.js, praca z npm (webpack, elixir)</p>
	</div>
	<div class="content">
		<p class="small-caps" big="true" name="att">Współpraca</p>
		<p>W sprawie współpracy zachęcam do kontaktu na email: <a href="mailto:kontakt.maciejkrol@gmail.com">kontakt.maciejkrol@gmail.com</a></p>
	</div>
</div>

@endsection