<!DOCTYPE html>
<html>
<head>
    @include('includes.head')
    <link rel="stylesheet" href="{{ pushStyle('css/gallery.css') }}">
</head>
<body>

<div id="wrapper">
    <div id="cookies"></div>
    <header>
        @include('includes.header')
    </header>

    <nav>
        @include('includes.nav')
    </nav>

    <main>
        <div id="mainContentWrapper">
            @yield('content')
        </div>
    </main>

    <footer>
        @include('includes.footer')
    </footer>
</div>
@include('includes.scripts')
</body>

</html>