@extends('layouts.main')

@section('content')
<div id="subContainer">
	<div id="subContainerPanel">
		@include($panel)
	</div>
	<div id="content">
		@yield('content2')
	</div>
</div>
@endsection