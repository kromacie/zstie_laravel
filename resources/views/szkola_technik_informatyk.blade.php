@extends('layouts.subsite', ['panel' => 'includes.school'])
@section('content2')
<div class="content">
    <p><span name="att">Technik informatyk</span> - opis zawodu</p><hr>
    <li>Opracowuje, uruchamia i obsługuje własne programy aplikacyjne, przystosowuje aplikacje firmowe na potrzeby zakładu pracy.</li>
    <li>Obsługuje komputery, posługując się gotowymi pakietami oprogramowania użytkowego i narzędziowego.</li>
    <li>Projektuje i wykonuje bazy danych i ich oprogramowanie aplikacyjne. Administruje bazami danych i systemami przetwarzania informacji.</li>
    <li>Dobiera konfigurację sprzętu i oprogramowania komputerowego. Obsługuje lokalne sieci komputerowe i nadzoruje ich pracę.</li>
    <li>Posługuje się językiem angielskim w stopniu umożliwiającym korzystanie z pisanej po angielsku dokumentacji oprogramowania i sprzętu oraz innymi językami obsługi wybranych rodzajów baz danych, w tym językiem SQL.</li>
    <li>Programuje w wybranych językach, w szczególności w Pascal, C++.</li>
    <li>Opracowuje algorytmy według podanych założeń, będących podstawą do samodzielnego wykonywania programów użytkowych.</li>
    <li>Uruchamia i obsługuje urządzenia peryferyjne systemu komputerowego.</li>
    <li>Usuwa uszkodzenia powstające w urządzeniach systemu komputerowego oraz testuje jakość ich pracy.</li>
    <li>Prowadzi prace serwisowe w punktach naprawy.</li>
    <li>Wykonuje rozliczenia kosztów wyrobów i usług.</li>
</div>
@endsection