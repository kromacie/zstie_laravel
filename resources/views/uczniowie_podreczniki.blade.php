@extends('layouts.subsite', ['panel' => 'includes.student'])
@section('content2')

<div class="content">
	<p>Podręczniki</p><hr>
	<li>Podręczniki dla <a href="{{ url('pdf/podreczniki_technikum.pdf') }}" target="_blank">Technikum nr 7</a> - plik w formacie PDF</li>
	<li>Podręczniki dla <a href="{{ url('pdf/podreczniki_zsz.pdf') }}" target="_blank">ZSZ nr 1</a> - plik w formacie PDF</li>
</div>

@endsection