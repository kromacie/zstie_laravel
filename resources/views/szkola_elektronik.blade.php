@extends('layouts.subsite', ['panel' => 'includes.school'])
@section('content2')

<div class="content">
    <p><span name="att">Elektronik</span> - opis zawodu</p><hr>
    <p>Absolwent szkoły kształcącej w zawodzie elektronik powinien być przygotowany do wykonywania następujących zadań zawodowych:
        <li>Montowanie elementów oraz układów elektronicznych na płytkach drukowanych w urządzeniach;</li>
        <li>Wykonywanie instalacji i instalowania urządzeń elektronicznych;</li>
        <li>Uruchamiania układów i instalacji na podstawie dokumentacji;</li>
        <li>Demontowania i przygotowania do recyclingu elementów, urządzeń i instalacji elektronicznych.</li>
        <li>Wykonuje konserwację i naprawy systemów komputerowych, obejmujące jednostki centralne i urządzenia peryferyjne, elektronicznych maszyn biurowych, sprzętu audiowizualnego oraz urządzeń radiokomunikacyjnych.</li>
        <li>Przeprowadza testowanie jednostki centralnej komputera, pamięci operacyjnej i urządzeń peryferyjnych. Wymienia pakiety, bloki i podzespoły w urządzeniach systemu komputerowego.</li>
        <li>Przeprowadza pomiary i usuwa uszkodzenia kart sieciowych linii transmisyjnych sieci komputerowych.</li>
        <li>Wykonuje montaż elementów i podzespołów elektronicznych, elektromechanicznych, wskaźników, układów zasilania, regulacji i sterowania.</li>
        <li>Posługuje się miernikami wielkości elektrycznych, miernikami elektronicznymi, generatorami przebiegów elektrycznych, oscyloskopami, specjalistycznymi przyrządami technologicznymi oraz miernikami natężenia pola elektromagnetycznego.</li>
        <li>Sprawdza działanie i współdziałanie mechanizmów i podzespołów elektromechanicznych w maszynach biurowych, urządzeniach radiokomunikacyjnych i sprzęcie audiowizualnym oraz dokonuje zabiegów konserwacyjnych lub wymiany elementów uszkodzonych.</li>
        <li>Przeprowadza strojenie, regulację i testowanie urządzeń po naprawie.</li>
        <li>Uczestniczy w procesie produkcji urządzeń elektronicznych i sprzętu sygnalizacyjnego przy montażu elementów oraz podzespołów elektronicznych.</li>
        <li>Uczestniczy w procesie produkcji elektronicznych urządzeń automatyki przemysłowej i instalacji systemów automatyki w obiektach przemysłowych.</li>
        <li>Wykonuje konserwację i naprawy rzutników, magnetofonów, magnetowidów, odtwarzaczy DVD, kamer i monitorów, wzmacniaczy, przetworników elektroakustycznych, odbiorników radiowych i telewizyjnych.</li>
    </p>
</div>
<div class="content">
    <p><span name="att">Elektronik</span> - informacje ogólne</p><hr>
    <p>Absolwent szkoły kształcącej w zawodzie elektronik powinien być przygotowany do wykonywania następujących zadań zawodowych:
    <li>Montowanie elementów oraz układów elektronicznych na płytkach drukowanych w urządzeniach;</li>
    <li>Wykonywanie instalacji i instalowania urządzeń elektronicznych;</li>
    <li>Uruchamiania układów i instalacji na podstawie dokumentacji;</li>
    <li>Demontowania i przygotowania do recyclingu elementów, urządzeń i instalacji elektronicznych.</li></p>

    <p><span name="att">Elektronicy</span> mogą pracować między innymi przy:
    <li>produkcji, konserwacji i naprawach wszelkiego sprzętu elektronicznego</li>
    <li>montażu  i konserwacji linii łączności telewizyjnej, radiowej i internetowej</li>
    <li>montażu i konserwacji systemów  monitoringu</li>
    <li>montażu i konserwacji systemów alarmowych"</li></p>
</div>

@endsection