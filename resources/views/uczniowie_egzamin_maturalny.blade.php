@extends('layouts.subsite', ['panel' => 'includes.student'])
@section('content2')


<div class="content">
    <p>Egzamin maturalny</p><hr>
    <li><a href="https://www.cke.edu.pl/images/_KOMUNIKATY/20150907_Komunikat_o_przyborach.pdf" target="_blank">Komunikat o przyborach 2017</a></li>
    <li><a href="https://www.cke.edu.pl/images/_KOMUNIKATY/20160819%20Harmonogram%20egzaminow%20w%202017%20r%20OGOLNE%20Fin.pdf" target="_blank">Harmonogram egzaminów 2017</a></li>
    <li><a href="https://www.cke.edu.pl/egzamin-maturalny/egzamin-w-nowej-formule/arkusze/2016-2/" target="_blank">Arkusze maturalne</a></li>
    <li><a href="https://oke.wroc.pl/?option=com_content&task=category&sectionid=10&id=150&Itemid=56" target="_blank">OKE Wrocław - informacje dla maturzystów</a></li>
    <li><a href="https://www.cke.edu.pl/images/stories/EFS/stawianie_sobie_celow.pdf" target="_blank">"Uczę się i umiem" czyli jak się uczyć do matury</a></li>
    <li><a href="https://www.cke.edu.pl/images/stories/EFS_1/jak_skut_zarz_swoim_czasem.pdf" target="_blank">"Jak skutecznie zarządzać swoim czasem"</a></li><br>
</div>
<div class="content">
    <p>Wyniki egzaminów</p><hr>
    <p>Egzamin Maturalny 2016</p>
    Zdawalność egzaminu maturalnego naszych uczniów w 2016 roku:<br>
    <li>technikum: 95,7%; średnia dla Wrocławia - 78,3%; średnia dla województwa - 70,3%</li>
    <hr>
    <p>Egzamin Maturalny 2015</p>
    Zdawalność egzaminu maturalnego naszych uczniów w 2015 roku:<br>
    <li>technikum: 91,7%; średnia dla Wrocławia - 74,1%</li>
    <hr>
    <p>Egzamin Maturalny 2014</p>
    Zdawalność egzaminu maturalnego naszych uczniów w 2014 roku:<br>
    <li>technikum: 90,2%; średnia dla Wrocławia - 78%</li>
    <hr>
    <p>Egzamin Maturalny 2013</p>
    Zdawalność egzaminu maturalnego naszych uczniów w 2013 roku:<br>

    <li>technikum: 88,4%; średnia dla Wrocławia - 78%</li>
    <li>Liceum profilowane: 75%; średnia dla Wrocławia - 63,2%</li>
    <hr>
    <p>Egzamin Maturalny 2012</p>
    Zdawalność egzaminu potwierdzającego kwalifikacje zawodowe w 2012 roku:<br>

    <li>technikum: 80,5%; średnia dla Wrocławia - 76,8%</li>
    <li>liceum profilowane: 76,9%; średnia dla Wrocławia - 64,1%</li>
    <hr>
    <p>Egzamin Maturalny 2011</p>
    Zdawalność egzaminu maturalnego naszych uczniów w 2011 roku:<br>

    <li>technikum: 91,1%; Miasto Wrocław 78,2%; woj. dolnośląskie 71,1%</li>
    <li>liceum profilowane: 71,1%; Miasto Wrocław 58,9%; woj. dolnośląskie 51,1%</li>
</div>

@endsection