@extends('layouts.subsite', ['panel' => 'includes.recruitment'])
@section('content2')
<div class="content">
    <p>Warunki rekrutacji do klas pierwszych</p><hr>
    <p><a href="{{ url('pdf/tabela_rekrutacja_2017.pdf') }}" target="_blank">Warunki rekrutacji</a> do klas I-wszych Zespołu Szkół Teleinformatycznych i Elektronicznych we Wrocławiu na rok szkolny 2017/2018.</p>
</div>
@endsection