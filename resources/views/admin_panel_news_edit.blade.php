@extends('layouts.main')

@section('extra_styles')
	<link rel="stylesheet" type="text/css" href="{{ push('css/editor.css', 'style') }}">
@endsection

@section('extra_scripts')
	<script src="{{ push('js/editor.js', 'script') }}"></script>
@endsection

@section('content')
	@include('includes.messages')
	<div id="editorWrapper">
		<form action="{{ route('news.edit.process') }}" id="editor" method="POST" enctype="multipart/form-data">
			@csrf
			<input type="hidden" name="id" value="{{ $event->id}}">
			<div class="formTitle">
				<p class="form-sentence small-caps">Tytuł artykułu: <span class="charRemain"></span></p>
				<input type="text" name="title" value="{{ old('title', $event->title) }}" placeholder="Wpisz tytuł..." maxLength=40 autofocus required>
			</div>
			<div class="formDescription">
				<p class="form-sentence small-caps">Krótki opis artykułu: <span class="charRemain"></span></p>
				<input type="text" name="description" value="{{ old('description', $event->description) }}" placeholder="Wpisz krótki opis..." maxLength=100 required>
			</div>
			<img src="{{ push('storage/' . $event->image, 'image') }}" alt="image" width="100%">
			<div class="formFile">
				<p class="form-sentence small-caps"><span>Zmień obrazek prezentujący artykuł.</span></p>
				<input name="image" type="file">
			</div>

			<div id="editorPanel">
				<div class="editorSection">
					<span class="editorValue"><i class="fas fa-font"></i></span>
					<select class="selectors" name="fontSize">
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
						<option value="5">5</option>
						<option value="6">6</option>
						<option value="7">7</option>
					</select>
					<select class="selectors" name="fontName">
						<option value="Times New Roman">Times New Roman</option>
						<option value="Lato">Lato</option>
						<option value="Arial">Arial</option>
					</select>
					<input class="selectors" type="color" name="foreColor">
				</div>
				<div class="editorSection">
					<span class="editorValue" title="paragraph" name="formatBlock"><i>P</i></span>
					<span class="editorValue" title="cofnij zmiany (ctrl+z)" name="undo"><i class="fas fa-undo"></i></span>
					<span class="editorValue" title="ponów zmiany (ctrl+y)" name="redo"><i class="fas fa-redo"></i></span>
				</div>
				<div class="editorSection">
					<span class="editorValue" title="pogrubienie" name="bold"><i class="fas fa-bold"></i></span>
					<span class="editorValue" title="kursywa" name="italic"><i class="fas fa-italic"></i></span>
					<span class="editorValue" title="podkreślenie" name="underline"><i class="fas fa-underline"></i></span>
					<span class="editorValue" title="przekreślenie" name="strike"><i class="fas fa-strikethrough"></i></span>
				</div>
				<div class="editorSection">
					<span class="editorValue" title="tekst z lewej" name="left"><i class="fas fa-align-left"></i></span>
					<span class="editorValue" title="tekst wycentrowany" name="center"><i class="fas fa-align-center"></i></span>
					<span class="editorValue" title="tekst z prawej" name="right"><i class="fas fa-align-right"></i></span>
					<span class="editorValue" title="tekst wyjustowany" name="justify"><i class="fas fa-align-justify"></i></span>
				</div>
				<div class="editorSection">
					<span class="editorValue" title="lista numerowana" name="numericList"><i class="fas fa-list-ol"></i></span>
					<span class="editorValue" title="lista nienumerowana" name="nonNumericList"><i class="fas fa-list-ul"></i></span>
				</div>
				<div class="editorSection">
					<span class="editorValue" title="odnośnik" name="link"><i class="fas fa-link"></i></span>
				</div>
				<div class="editorSection">
					<span class="editorValue small-caps" title="generator (zalecane)" name="generate">Wygeneruj treść</span>
				</div>
			</div>

			<div id="resultText" contenteditable="true">{!! old('content', $event->content) !!}</div>

			<hr class="formArticleBreaker">
			<div class="checkbox"><input id="eventAdd" name="is_event" {{ $event->is_event ? 'checked' : ''}} type="checkbox"> <span class="small-caps"><label for="eventAdd">Oznacz jako wydarzenia (<span name="att">doda odnośnik w kalendarzu wydarzeń</span>)</label></span>
				<input id="eventDate" name="calendar_date" type="date" placeholder="DD/MM" value="{{ $event->calendar_date->format('Y-m-d') }}">
			</div>
			<div class="checkbox"><input id="sliderAdd" name="is_slide" {{ $event->is_slide ? 'checked': '' }} type="checkbox"> <span class="small-caps"><label for="sliderAdd">Dodaj do pokazu slajdów (<span name="att">zalecane, jeżeli wydarzenie</span>)</label></span></div>
			<div class="checkbox"><input id="importantAdd" name="is_important" {{ $event->is_important ? 'checked': '' }} type="checkbox"> <span class="small-caps"><label for="importantAdd">Oznacz jako ważne (opcjonalne)</label></span></div>
			@can('publish articles')
				<div class="checkbox"><input id="importantAdd" name="is_published" {{ $event->is_published ? 'checked': '' }} type="checkbox"> <span class="small-caps"><label for="importantAdd">Opublikuj</label></span></div>
			@endcan
			<hr class="formArticleBreaker">
			<input type="submit" value="Dodaj artykuł">
		</form>
	</div>
@endsection
