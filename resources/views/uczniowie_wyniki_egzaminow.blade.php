@extends('layouts.subsite', ['panel' => 'includes.student'])
@section('content2')

<div class="content">
    <p>Egzaminy maturalne ( <a href="{{ route('students.exam.mature') }}">więcej</a> )</p>
    <li><a href="https://www.cke.edu.pl/images/_KOMUNIKATY/20150907_Komunikat_o_przyborach.pdf" target="_blank">Komunikat o przyborach 2017</a></li>
    <li><a href="https://www.cke.edu.pl/images/_KOMUNIKATY/20160819%20Harmonogram%20egzaminow%20w%202017%20r%20OGOLNE%20Fin.pdf" target="_blank">Harmonogram egzaminów 2017</a></li>
    <li><a href="https://www.cke.edu.pl/egzamin-maturalny/egzamin-w-nowej-formule/arkusze/2016-2/" target="_blank">Arkusze maturalne</a></li>
    <li><a href="https://oke.wroc.pl/?option=com_content&task=category&sectionid=10&id=150&Itemid=56" target="_blank">OKE Wrocław - informacje dla maturzystów</a></li>
    <li><a href="https://www.cke.edu.pl/images/stories/EFS/stawianie_sobie_celow.pdf" target="_blank">"Uczę się i umiem" czyli jak się uczyć do matury</a></li>
    <li><a href="https://www.cke.edu.pl/images/stories/EFS_1/jak_skut_zarz_swoim_czasem.pdf" target="_blank">"Jak skutecznie zarządzać swoim czasem"</a></li><br><hr>
    <p>Egzaminy zawodowe ( <a href="{{ route('students.exam.tech') }}">więcej</a> )</p>
    <li><a href="http://egzamin-zawodowy.edu.pl/" target="_blank">Trenażer umiejętności zdania egzaminu zawodowego</a></li>
    <li><a href="http://egzamin-informatyk.pl/e12-e13-e14-testy-online-egzamin-zawodowy" target="_blank">Testy egzaminacyjne E12, E13, E14</a></li><br>
</div>
<div class="content">
    <p>Wyniki egzaminów</p><hr>
    <p>Egzamin Maturalny 2016</p><hr>
    Zdawalność egzaminu maturalnego naszych uczniów w 2016 roku:<br>
    <li>technikum: 95,7%; średnia dla Wrocławia - 78,3%; średnia dla województwa - 70,3%</li><hr>
    <p>Egzamin zawodowy 2016</p><hr>
    Wyniki egzaminu zawodowego w sesji letniej 2016 r. w pliku <a href="{{ url('pdf/egz_zaw_lato_2016.pdf') }}" target="_blank">PDF</a><hr>
    <p>Egzamin Maturalny 2015</p><hr>
    Zdawalność egzaminu maturalnego naszych uczniów w 2015 roku:<br>
    <li>technikum: 91,7%; średnia dla Wrocławia - 74,1%</li><hr>
    <p>Egzamin zawodowy 2015</p><hr>
    Zdawalność egzaminu potwierdzającego kwalifikacje w zawodzie w 2015 - technikum:<br>
    <li>kwalifikacja E.05: 40%; średnia dla Wrocławia - 40%; średnia dla województwa - 40%</li>
    <li>kwalifikacja E.09: 72,7%; średnia dla Wrocławia - 72,7%; średnia dla województwa - 72,7%</li>
    <li>kwalifikacja E.12: 89,6%; średnia dla Wrocławia - 87,6%; średnia dla województwa - 84%</li>
    <li>kwalifikacja E.15: 50%; średnia dla Wrocławia - 50%; średnia dla województwa - 50%</li>
</div>

@endsection