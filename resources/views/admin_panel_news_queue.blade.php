@extends('layouts.main')
@section('content')
<div class="content news_queue">
    @include('includes.messages')
	<p><strong>Lista artykułów:</strong></p>
	@foreach($news as $event)
	<li noselect>
		<span>{{ $event->title }}</span>
		<span name="att">{{ $event->user->name }}</span>
		<div class="news_que_panel">
			<a href="{{ route('news.delete', ['id' => $event->id]) }}" name="remove"><i title="usuń" class="far fa-trash-alt"></i></a>
			<a href="{{ route('news.edit', ['id' => $event->id]) }}" name="edit"><i title="edytuj" class="far fa-edit"></i></a>
			<a href="{{ route('news.show', ['id' => $event->id]) }}" name="preview"><i title="podgląd" class="fas fa-eye"></i></a>
			<a href="#" onclick="publish({{$event->id}})" name="accept"><i title="akceptuj" class="fas fa-check"></i></a>
		</div>
	</li>
	<form id="item_{{ $event->id }}" style="visibility: hidden;" method="post" action="{{ route('news.publish', ['id' => $event->id]) }}">
		@csrf
		<input type="hidden" name="id" value="{{ $event->id }}">
	</form>
	@endforeach
</div>
@endsection

@section('extra_scripts')

<script type="text/javascript">
	function publish(a)
	{
		$('#item_' + a).submit();
	}
</script>

@endsection