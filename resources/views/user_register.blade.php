@extends('layouts.main')

@section('content')
  @include('includes.messages')
  <form id="formSignReg" action="{{ route('register') }}" method="post">
    @csrf
    <div id="formWrapper">
      <table id="formTable">
        <tr>
          <td>
            <p class="formSentence">Email użytkownika:</p>
          </td>
          <td><input type="email" name="email" value="{{ old('email') }}" required><br><span class="formMessage small-caps">Upewnij się, że podałeś prawidłowy adres e-mail, będzie służył do logowania</span></td>
        </tr>
        <tr>
          <td>
            <p class="formSentence">Nazwa użytkownika:</p>
          </td>
          <td><input type="text" name="name" value="{{ old('name') }}" autofocus required><br><span class="formMessage small-caps">Nazwa użytkownika powinna zawierać od 6 do 18 znaków alfanumerycznych</span></td>
        </tr>
        <tr>
          <td>
            <p class="formSentence">Wprowadź hasło:</p>
          </td>
          <td><input type="password" name="password" required><br><span class="formMessage small-caps">Hasło powinno zawierać od 8 do 20 znaków alfanumerycznych</span></td>
        </tr>
        <tr>
          <td>
            <p class="formSentence">Powtórz hasło:</p>
          </td>
          <td><input type="password" name="password2" required><br><span class="formMessage small-caps">Oba hasła muszą być zgodne</span></td>
        </tr>
      </table>
      <p class="formSentence">Wybierz poziom zarządzania: </p>
      <hr>
      @foreach($roles as $role)
        <label class="formOption small-caps" for="{{ $role->name }}"><input id="{{ $role->name }}" type="radio" name="role" value="{{ $role->name }}"><span>{{ $role->name }}</span></label>
      @endforeach
      <hr>
      <div class="checkbox">
        <input id="termsId" type="checkbox" {{ filled('terms') ? 'checked' : '' }} name="terms" required> <span><label for="termsId" class="small-caps">Rejestrując się akceptujesz <a href="#">regulamin</a></label></span>
      </div>
      <input type="submit" value="Zarejestruj">
    </div>
  </form>
@endsection
