@extends('layouts.subsite', ['panel' => 'includes.recruitment'])
@section('content2')
<div class="content">
	<p>Nauczane języki</p><hr>
	<p>Na podstawie Zarządzenia dyrektora Nr 4/2013:<br>§ 10 - W szkole prowadzone sę zajęcia z zakresu języka obcego nowożytnego: <span name="att">języka angielskiego</span> i <span name="att">języka niemieckiego</span>. Przydział do grup ze względu na kontynuację i stopień zaawansowania następuje na podstawie zapisu na świadectwie ukończenia gimnazjum.</p>
</div>
@endsection