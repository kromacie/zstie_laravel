@extends('layouts.subsite', ['panel' => 'includes.school'])
@section('content2')
<div class="content">
    <p><span name="att">Technik elektronik</span> - opis zawodu</p><hr>
    <p>
     <li>przeprowadzanie kontroli technicznej we wszystkich fazach produkcji,</li>
     <li>uruchamianie i testowanie urządzeń elektronicznych,</li>
     <li>nadzorowanie i kontrola pracy urządzeń elektronicznych,</li>
     <li>naprawa urządzeń elektronicznych.</li>
    </p>
</div>
@endsection