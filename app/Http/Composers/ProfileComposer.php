<?php namespace App\Http\Composers;

use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class ProfileComposer
{

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        if(Auth::check())
        {
            $view->with('user', Auth::user());
        }

    }
}