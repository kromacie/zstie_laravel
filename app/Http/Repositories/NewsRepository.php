<?php namespace App\Http\Repositories;

use App\Http\Requests\CreateNewsRequest;
use App\Http\Requests\EditNewsRequest;
use App\Models\News;

class NewsRepository implements NewsRepositoryInterface
{

    protected $news;

    /**
     * NewsRepository constructor.
     * @param News $news
     */
    public function __construct(News $news)
    {
        $this->news = $news;
    }

    public function getPublishedEvents()
    {
        return $this->news->where([
            ['is_event', '=', '1', 'AND'],
            ['is_published', '=', '1']
        ])->get();
    }

    public function getUnpublishedEvents()
    {
        return $this->news->where([
            ['is_event', '=', '1', 'AND'],
            ['is_published', '=', '0']
        ])->get();
    }

    public function getPublishedNews()
    {
        return $this->news->where([
            ['is_published', '=', '1']
        ])->get();
    }

    public function getUnpublishedNews()
    {
        return $this->news->where([
            ['is_published', '=', '0']
        ])->get();
    }

    public function create(CreateNewsRequest $request)
    {
        $path = $request->file('image')->store('news', 'public');
        $news = $this->news;
        $news->image = $path;


        if(auth()->user()->can('publish articles')){
            $news->is_important = $request->filled('is_important');
        } else {
            $news->is_published = false;
        }

        $news->is_published = $request->filled('is_published');
        if($request->filled('is_event')){
            $news->is_event = true;
            $news->calendar_date = $request->input('calendar_date');
        } else {
            $news->is_event = false;
        }
        $news->is_slide = $request->filled('is_slide');
        $news->fill([
            config('app.fallback_locale') => [
                'title' => $request->input('title'),
                'description' => $request->input('description'),
                'content' => $request->input('content')
            ]
        ]);
        $news->user_id = auth()->id();
        $news->save();
    }

    public function getById($id)
    {
        return $this->news->findOrFail($id);
    }

    public function edit(EditNewsRequest $request)
    {

        $news = $this->news->findOrFail($request->validated()['id']);

        if($request->has('image')){
            $path = $request->file('image')->store('news', 'public');
            $news->image = $path;
        }

        if(auth()->user()->can('publish articles'))
        {
            $news->is_published = $request->filled('is_published');
        }
        $news->is_important = $request->filled('is_important');

        if($request->filled('is_event')){
            $news->is_event = true;
            $news->calendar_date = $request->input('calendar_date');
        } else {
            $news->is_event = false;
        }
        $news->is_slide = $request->filled('is_slide');

        $translation = $news->translate();
        $translation->description = $request->validated()['description'];
        $translation->title = $request->validated()['title'];
        $translation->content = $request->validated()['content'];
        $translation->save();

        $news->save();

    }

    public function delete($id)
    {
        return $this->news->findOrFail($id)->delete();
    }

    public function publish($id)
    {
        $news = $this->getById($id);
        $news->is_published = true;
        return $news->save();
    }
}