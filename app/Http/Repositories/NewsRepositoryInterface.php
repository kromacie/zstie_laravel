<?php namespace App\Http\Repositories;

use App\Http\Requests\CreateNewsRequest;
use App\Http\Requests\EditNewsRequest;

interface NewsRepositoryInterface
{
    public function getPublishedEvents();

    public function getUnpublishedEvents();

    public function getPublishedNews();

    public function getUnpublishedNews();

    public function getById($id);

    public function delete($id);

    public function create(CreateNewsRequest $request);

    public function edit(EditNewsRequest $request);

    public function publish($id);
}