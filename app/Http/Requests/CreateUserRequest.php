<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|unique:users',
            'name' => 'required|string|min:6|max:18',
            'password' => 'required_with:password2|string|min:8|max:20|same:password2',
            'password2' => 'min:8',
            'role' => 'required',
            'terms' => 'required|accepted'
        ];
    }
}
