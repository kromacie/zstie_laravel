<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditNewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();

        $validator->sometimes('calendar_date', 'required|date_format:Y-m-d', function ($input){
            return $input->is_event == 'on';
        });

        return $validator;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|exists:news',
            'title' => 'required|max:50',
            'description' => 'required|max:100',
            'content' => 'required|max:10000',
            'is_event' => 'required|sometimes',
            'is_slide' => 'required|sometimes',
            'is_important' => 'required|sometimes',
            'is_published' => 'required|sometimes',
            'calendar_date' => 'sometimes',
            'image' => 'required|sometimes|mimes:jpeg,jpg,bmp,png,JPEG|max:1024'
        ];
    }
}
