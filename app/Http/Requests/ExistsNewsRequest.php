<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ExistsNewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|exists:news'
        ];
    }

    public function all($keys = null)
    {
        return array_replace_recursive(
            parent::all($keys),
            $this->route()->parameters()
        );
    }
}
