<?php

namespace App\Http\Controllers;

use App\Http\Repositories\NewsRepository;
use App\Models\News;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @param NewsRepository $repository
     * @return \Illuminate\Http\Response
     */
    public function index(NewsRepository $repository)
    {
        return view('main')->with(
            [
                'news' => $repository->getPublishedNews(),
                'events' => $repository->getPublishedEvents()
            ]
        );
    }
}
