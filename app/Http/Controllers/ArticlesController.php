<?php

namespace App\Http\Controllers;

use App\Http\Repositories\NewsRepository;
use App\Http\Requests\CreateNewsRequest;
use App\Http\Requests\EditNewsRequest;
use App\Http\Requests\ExistsNewsRequest;
use Illuminate\Support\Facades\Auth;

class ArticlesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('showNews');
    }


    public function showCreateNewsForm()
    {
        return view('user_news_add');
    }

    public function publishNews(ExistsNewsRequest $request, NewsRepository $repository)
    {
        $repository->publish($request->validated()['id']);
        return redirect()->route('news.unpublished.show')->with('success', 'Dodano pomyślnie!');
    }

    public function createNews(CreateNewsRequest $request, NewsRepository $repository)
    {
        $repository->create($request);
        return redirect()->route('home')->with('success', 'Dodano pomyślnie!');
    }

    public function showUnpublishedNews(NewsRepository $repository)
    {
        return view('admin_panel_news_queue')->with('news', $repository->getUnpublishedNews());
    }

    public function showDeleteNewsForm(ExistsNewsRequest $request, NewsRepository $repository)
    {
        return view('admin_panel_news_delete')->with('news', $repository->getById($request->validated()['id']));
    }

    public function deleteNews(ExistsNewsRequest $request, NewsRepository $repository)
    {
        $repository->delete($request->validated()['id']);
        return redirect()->route('news.unpublished.show')->with('success', 'Usunięto pomyślnie!');
    }

    public function showEditNewsForm(ExistsNewsRequest $request, NewsRepository $repository)
    {
        return view('admin_panel_news_edit')->with('event', $repository->getById($request->validated()['id']));
    }

    public function editNewsProcess(EditNewsRequest $request, NewsRepository $repository)
    {
        $repository->edit($request);
        return redirect()->route('news.unpublished.show')->with('success', 'Edytowano pomyślnie!');
    }

    public function showAllNews(NewsRepository $repository)
    {
        return view('nowosci')->with('news', $repository->getPublishedNews());
    }

    public function showNews(ExistsNewsRequest $request, NewsRepository $repository)
    {

        $article = $repository->getById($request->validated()['id']);

        if (!$article->is_published) {
            if (Auth::check() && Auth::user()->can('show articles')) {
                return view('news_show')->with('news', $article);
            } else {
                return redirect()->back()->withErrors('permission', 'Nie masz uprawnień żeby przeglądać ten artykuł!');
            }
        }

        return view('news_show')->with('news', $article);
    }



}
