<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DefaultController extends Controller
{
    public function recruitment()
    {
    	return view('rekrutacja');
    }

    public function recruitmentLanguages()
    {
    	return view('rekrutacja_nauczane_jezyki');
    }

    public function recruitmentDoctor()
    {
    	return view('rekrutacja_skierowania_lekarskie');
    }

    public function recruitmentConditions()
    {
    	return view('rekrutacja_warunki_przyjecia');
    }

    public function school()
    {
    	return view('szkola');
    }

    public function schoolHistory()
    {
    	return view('szkola_historia_szkoly');
    }

    public function schoolPatrons()
    {
    	return view('szkola_nasi_patroni');
    }

    public function schoolGallery()
    {
    	return view('galeria');
    }

    public function schoolContact()
    {
    	return view('kontakt');
    }

    public function schoolIT()
    {
    	return view('szkola_technik_informatyk');
    }

    public function schoolET()
    {
    	return view('szkola_technik_elektronik');
    }

    public function schoolE()
    {
    	return view('szkola_elektronik');
    }

    public function schoolTeachers()
    {
    	return view('szkola_kadra_pedagogiczna');
    }

    public function schoolParents()
    {
    	return view('szkola_rada_rodzicow');
    }

    public function schoolPupils()
    {
    	return view('404');
    }

    public function schoolLibrary()
    {
    	return view('szkola_biblioteka');
    }

    public function students()
    {
    	return view('uczniowie');
    }

    public function studentsLaws()
    {
    	return view('uczniowie_akty_prawne');
    }

    public function studentsExamMature()
    {
    	return view('uczniowie_egzamin_maturalny');
    }

    public function studentsExamTech()
    {
    	return view('uczniowie_egzamin_zawodowy');
    }

    public function studentsCalendarEvents()
    {
    	return view('uczniowie_kalendarz_imprez');
    }

    public function studentsCalendarYear()
    {
    	return view('uczniowie_kalendarz_roku');
    }

    public function studentsBooks()
    {
    	return view('uczniowie_podreczniki');
    }

    public function studentsProjects()
    {
    	return view('uczniowie_projekty');
    }

    public function studentsTenders()
    {
    	return view('uczniowie_przetargi');
    }

    public function studentsLessons()
    {
    	return view('uczniowie_zajecia_pozalekcyjne');
    }

    public function studentsExamResults()
    {
        return view('uczniowie_wyniki_egzaminow');
    }

    public function eventsSchool()
    {
    	return view('events.szkola_w_miescie');
    }

    public function eventsChange()
    {
    	return view('events.wymiana_mlodziezy_z_eckener_schule_we_flensburgu');
    }

    public function eventsVolunter()
    {
    	return view('events.wolontariat');
    }

    public function schoolGalleryZstie()
    {
        return view('gallery.gallery_zstie');
    }

    public function aboutUs()
    {
        return view('behind_o_nas');
    }

    public function creators()
    {
        return view('behind_tworcy');
    }

}
