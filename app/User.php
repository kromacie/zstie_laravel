<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 06 Jul 2018 10:28:46 +0000.
 */

namespace App;

use Illuminate\Notifications\Notifiable;
use Reliese\Database\Eloquent\Model as Eloquent;
use Spatie\Permission\Traits\HasRoles;

/**
 * Class User
 * 
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $news
 *
 * @package App\Models
 */
class User extends \Illuminate\Foundation\Auth\User
{

    use Notifiable;
    use HasRoles;

    protected $hidden = [
		'password',
		'remember_token'
	];

	protected $fillable = [
		'name',
		'email',
		'password',
		'remember_token'
	];

	public function news()
	{
		return $this->hasMany(\App\Models\News::class);
	}
}
