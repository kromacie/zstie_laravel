<?php

if (!function_exists('push')) {

    /**
     * @param string $resourcePath
     * @param $type
     * @return mixed
     */
    function push(string $resourcePath, $type)
    {
        app('server-push')->queueResource($resourcePath, $type);
        return asset($resourcePath);
    }
}