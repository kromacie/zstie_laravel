<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 06 Jul 2018 03:00:53 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Language
 * 
 * @property string $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $news_translations
 *
 * @package App\Models
 */
class Language extends Eloquent
{
	public $incrementing = false;

	public function news_translations()
	{
		return $this->hasMany(\App\Models\NewsTranslation::class);
	}
}
