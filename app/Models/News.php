<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 06 Jul 2018 04:08:38 +0000.
 */

namespace App\Models;

use App\Models\NewsTranslation;
use Dimsav\Translatable\Translatable;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class News
 * 
 * @property int $id
 * @property string $image
 * @property bool $is_event
 * @property bool $is_slide
 * @property bool $is_important
 * @property bool $is_published
 * @property \Carbon\Carbon $calendar_date
 * @property int $user_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\User $user
 * @property \Illuminate\Database\Eloquent\Collection $news_translations
 *
 * @package App\Models
 */
class News extends Eloquent
{

    use Translatable;

	protected $casts = [
		'is_event' => 'bool',
		'is_slide' => 'bool',
		'is_important' => 'bool',
		'is_published' => 'bool',
		'user_id' => 'int'
	];

	protected $dates = [
		'calendar_date'
	];

	protected $fillable = [
		'image',
		'is_event',
		'is_slide',
		'is_important',
		'is_published',
		'calendar_date',
		'user_id'
	];

    public $translatedAttributes = ['title', 'description', 'content'];
    public $translationModel = 'App\Models\NewsTranslation';

	public function user()
	{
		return $this->belongsTo(\App\User::class);
	}

	public function news_translations()
	{
		return $this->hasMany(NewsTranslation::class);
	}

}
