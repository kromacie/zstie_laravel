<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 06 Jul 2018 03:01:10 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class NewsTranslation
 * 
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $content
 * @property string $language_id
 * @property int $news_id
 * 
 * @property \App\Models\Language $language
 * @property \App\Models\News $news
 *
 * @package App\Models
 */
class NewsTranslation extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'news_id' => 'int'
	];

	protected $fillable = [
		'title',
		'description',
		'content',
		'language_id',
		'news_id'
	];

	public function language()
	{
		return $this->belongsTo(\App\Models\Language::class);
	}

	public function news()
	{
		return $this->belongsTo(\App\Models\News::class);
	}
}
