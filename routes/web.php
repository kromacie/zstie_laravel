<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', 'HomeController@index')->name('home');
Route::get('/logowanie', 'Auth\\LoginController@showLoginForm')->name('login');
Route::get('/rejestracja', 'Auth\\RegisterController@showRegistrationForm')->name('register');
Route::post('/rejestracja', 'Auth\\RegisterController@register')->name('register.process');
Route::post('/logowanie', 'Auth\\LoginController@login')->name('login.process');
Route::get('/wylogowanie', 'Auth\\LoginController@logout')->name('logout');
Route::get('/profil', 'UserController@profile')->name('profile');

Route::get('/nowosci/dodaj', 'ArticlesController@showCreateNewsForm')->name('news.create')->middleware('permission:create articles');
Route::post('/nowosci/dodaj', 'ArticlesController@createNews')->name('news.create.process')->middleware('permission:create articles');
Route::get('/nowosci/kolejka', 'ArticlesController@showUnpublishedNews')->name('news.unpublished.show')->middleware('permission:publish articles');
Route::post('/nowosci/zaakceptuj/{id}', 'ArticlesController@publishNews')->name('news.publish')->middleware('permission:publish articles');
Route::get('/nowosci/usun/{id}', 'ArticlesController@showDeleteNewsForm')->name('news.delete')->middleware('permission:delete articles');
Route::post('/nowosci/usun', 'ArticlesController@deleteNews')->name('news.delete.process')->middleware('permission:delete articles');
Route::get('/nowosci/edytuj/{id}', 'ArticlesController@showEditNewsForm')->name('news.edit')->middleware('permission:edit articles');
Route::post('/nowosci/edytuj', 'ArticlesController@editNewsProcess')->name('news.edit.process')->middleware('permission:edit articles');
Route::get('/nowosci/{id}', 'ArticlesController@showNews')->name('news.show')->middleware();
Route::get('/nowosci', 'ArticlesController@showAllNews')->name('news')->middleware();

Route::get('/rekrutacja', 'DefaultController@recruitment')->name('recruitment')->middleware();
Route::get('/rekrutacja/nauczane_jezyki', 'DefaultController@recruitmentLanguages')->name('recruitment.languages')->middleware();
Route::get('/rekrutacja/warunki_przyjecia', 'DefaultController@recruitmentConditions')->name('recruitment.conditions')->middleware();
Route::get('/rekrutacja/skierowania_lekarskie', 'DefaultController@recruitmentDoctor')->name('recruitment.doctor')->middleware();

Route::get('/szkola', 'DefaultController@school')->name('school')->middleware();
Route::get('/szkola/historia', 'DefaultController@schoolHistory')->name('school.history')->middleware();
Route::get('/szkola/nasi_patroni', 'DefaultController@schoolPatrons')->name('school.patrons')->middleware();
Route::get('/szkola/galeria', 'DefaultController@schoolGallery')->name('school.gallery')->middleware();
Route::get('/szkola/kontakt', 'DefaultController@schoolContact')->name('school.contact')->middleware();
Route::get('/szkola/technik_informatyk', 'DefaultController@schoolIT')->name('school.it')->middleware();
Route::get('/szkola/technik_elektronik', 'DefaultController@schoolET')->name('school.et')->middleware();
Route::get('/szkola/elektronik', 'DefaultController@schoolE')->name('school.e')->middleware();
Route::get('/szkola/kadra_pedagogiczna', 'DefaultController@schoolTeachers')->name('school.teachers')->middleware();
Route::get('/szkola/samorzad_uczniowski', 'DefaultController@schoolPupils')->name('school.pupils')->middleware();
Route::get('/szkola/rada_rodzicow', 'DefaultController@schoolParents')->name('school.parents')->middleware();
Route::get('/szkola/biblioteka', 'DefaultController@schoolLibrary')->name('school.library')->middleware();

Route::get('/uczniowie', 'DefaultController@students')->name('students')->middleware();
Route::get('/uczniowie/akty_prawne', 'DefaultController@studentsLaws')->name('students.laws')->middleware();
Route::get('/uczniowie/egzamin_maturalny', 'DefaultController@studentsExamMature')->name('students.exam.mature')->middleware();
Route::get('/uczniowie/egzamin_zawodowy', 'DefaultController@studentsExamTech')->name('students.exam.tech')->middleware();
Route::get('/uczniowie/kalendarz_imprez', 'DefaultController@studentsCalendarEvents')->name('students.calendar.events')->middleware();
Route::get('/uczniowie/kalendarz_roku', 'DefaultController@studentsCalendarYear')->name('students.calendar.year')->middleware();
Route::get('/uczniowie/podreczniki', 'DefaultController@studentsBooks')->name('students.books')->middleware();
Route::get('/uczniowie/projekty', 'DefaultController@studentsProjects')->name('students.projects')->middleware();
Route::get('/uczniowie/przetargi', 'DefaultController@studentsTenders')->name('students.tenders')->middleware();
Route::get('/uczniowie/wyniki_egzaminów', 'DefaultController@studentsExamResults')->name('students.exam.results')->middleware();
Route::get('/uczniowie/zajecia_pozalekcyjne', 'DefaultController@studentsLessons')->name('students.lessons')->middleware();

Route::get('/wydarzenia/wymiana', 'DefaultController@eventsChange')->name('events.change')->middleware();
Route::get('/wydarzenia/wolontariat', 'DefaultController@eventsVolunter')->name('events.volunter')->middleware();
Route::get('/wydarzenia/szkola', 'DefaultController@eventsSchool')->name('events.school')->middleware();

Route::get('/kontakt', 'DefaultController@schoolContact')->name('contact')->middleware();

Route::get('/szkola/galeria/zstie', 'DefaultController@schoolGalleryZstie')->name('school.gallery.zstie')->middleware();

Route::get('/o_nas', 'DefaultController@aboutUs')->name('about.us')->middleware();
Route::get('/ciasteczka', 'DefaultController@cookies')->name('cookies')->middleware();
Route::get('/tworcy', 'DefaultController@creators')->name('creators')->middleware();