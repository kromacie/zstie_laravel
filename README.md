# Zstie with Laravel

Strona zrobiona dla ZSTiE

## Jak uruchomić?

Upewnij się że masz zainstalowaną najnowszą wersję composera, node.js oraz npm.

Jako pierwszą, w root directory naszego projektu, należy wykonać

```
composer install
```

Ta komenta pobierze wszystkie zależności które są wykorzystywane przy projekcie.

Następnie, należy wykonać:

```
npm i
```

również w root directory naszego projektu.

Skopiuj plik env.example i zmień jego nazwę na .env
Kolejnym krokiem będzie wygenerowanie klucza:

```
php artisan key:generate
```
A następnie skonfigurowanie połączenia z bazą danych w pliku .env
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1 (host)
DB_PORT=3306 (port)
DB_DATABASE=zstie (baza danych która musi już istnieć)
DB_USERNAME=root (login)
DB_PASSWORD= (hasło, w przypadku braku, zostawiamy puste)
```

Po czym wykonujemy komendę:
```
php artisan config:cache
```

Tworzymy tabele w naszej bazie komendą:
```
php artisan migrate
```

Oraz wypełniamy niezbędnymi danymi:
```
php artisan db:seed
```

Tworzymy dostęp do katalogu storage, aby działało przechowywanie i wyświetlanie obrazków (potrzebne dla newsów), komendą:

```
php artisan storage:link
```

I na koniec odpalamy serwer komendą
```
php artisan serve
```

Hasło oraz login dla wygenerowanych kont, dostępne są w folderze: /root/database/seeders/UsersSeeder.php

Serwer domyslnie uruchamia się pod adresem: http://localhost:8000